<? $filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");
?>
<div style="" id="allblock">
	<div class="login_box">
    	<div class="logon_box_sub">
        	<div id="ad_left_sd">
            	<div class="logo_holder">
                    <div class="logo_holder_cont">
                        <div class="logo_holder_cont_img_cont"><img src="../images/logo.png" style="" /></div>
                        <div class="administr_text">adminisztráció</div>
                    </div>
             	</div>
            </div>
            <div style="" id="ad_rgt_sd">
                <? if($hibauzi_login != ""){ ?>
                     <div class="megerdel_block">
                         <div class="logon_block_left"><span><?= $hibauzi_login ?></span></div>
                     </div>                
                <? } ?> 
            	<form method="POST" target="_self" name="login" id="login" enctype="multipart/form-data" style="margin:0px; padding:0px;" onsubmit="return elkuld_regel()">
                     <div class="megerdel_block">
                         <div class="logon_block_left">Felhasználó: <span id="user_sapn"></span></div>
                         <div class="logon_block_rgt" ><input type="text" name="user_neve" id="user_neve" value="" class="rounded_logi" ></div>
                     </div>
                     <div class="megerdel_block">
                         <div class="logon_block_left">jelszó: <span id="pass_sapn"></span></div>
                         <div class="logon_block_rgt" ><input type="password" name="user_pass" id="user_pass" value="" class="rounded_logi" ></div>
                     </div>
                     <div class="megerdel_block" style="text-align:left;">
                         <div class="logon_block_allwidth" ><input type="submit" value="belépés &#xf054;" class="button_login" /></div>
                     </div>
                </form> 
            </div>
        </div>    
    </div>
</div>



<script>
var left_side = 0
$( document ).ready(function() {
	$("#allblock").height(myHeight);
	left_side = $("#ad_rgt_sd").height();
	$("#ad_left_sd").height(left_side);
	//$("#allblock").html(myHeight);
});
$("#user_neve").click(function() {
  $("#user_sapn").html("");
});
$("#user_pass").click(function() {
  $("#pass_sapn").html("");
});
$(window).resize(function(){
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
	//alert(magas);
	$("#allblock").height(myHeight);
	//$("#allblock").html(myHeight);
});

function elkuld_regel(){
	if (document.forms["login"].user_neve.value == "" ){
		document.forms["login"].user_neve.focus();
		$("#user_sapn").html("kötelező mező &#xf063;");
		return false;
	}
	if (document.forms["login"].user_pass.value == "" ){
		document.forms["login"].user_pass.focus();
		$("#pass_sapn").html("kötelező mező &#xf063;");
		return false;
	}
	document.forms["login"].submit();
}

</script>
