<?
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");
?>
<style>
@media only screen and (max-width: 3000px) {
.allbottom_cont{width:100%; text-align:center; display:inline-block; background-color:#183828;}
.allbottom_cont_ins{width:99%; max-width:1600px; margin:0px auto;}



.bottom_big_icons_logo_allcont{width:100%;display:inline-block;margin:0px auto;margin-bottom:20px; margin-top:20px;text-align:center; border-top:1px solid #4b4b4b;}
.bottom_big_icons_logo{display: inline-block; margin:0.5%; padding-top:20px;}
.bottom_big_icons_logo img{width:100%; max-width:100%; height:auto;margin:0px auto; border:0px;}


.bottom_big_icons_logo:hover .bottom_big_icons_logo_brounds{background-color:#FFF; border:2px solid #1d1d1d;}
.bottom_big_icons_logo:hover .bottom_big_icons_logo_brounds .bottom_big_icons_logo_brounds_ins{ color:#383838;}

.allbottom_cont_footer_yera{width:100%; margin:0px 0px 20px 0px; text-align:center;font-family: 'Playfair Display', sans-serif;font-size:17px; color:#FFF; font-weight:400;text-transform:uppercase;}

.allbottom_cont_feliart_cont{width:100%; margin:40px 0px 0px 0px; text-align:left;}
.botton_tvkeszthely_logo{width:auto; float:left;font-family: 'Playfair Display', sans-serif;color:#FFF;font-size:28px; line-height:14px; font-style:normal; text-transform:uppercase; display:inline-block;}
.botton_tvkeszthely_logo_left{width:auto; float:left; margin:0px 0px 0px 30px;}
.botton_tvkeszthely_logo_left ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
	margin:-8px 0px 0px 0px;
	position:relative;
}

.botton_tvkeszthely_logo_left li {
    float: left;
}

.botton_tvkeszthely_logo_left li a {
    display: block;
    text-align: center;
    padding: 2px 4px 2px 4px;
	margin:0px 0px 0px 10px;
    text-decoration: none;
	-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;
	font-family: 'Playfair Display', sans-serif;font-size:17px; color:#FFF; font-weight:400; text-transform:uppercase;
}
.botton_tvkeszthely_logo_left li a:hover {
    background-color: #707070;
	color: #FFF;
}
.bottom_big_icons_logo_brounds{width:70px; height:70px; position:relative; border-radius:100%; background-color:#383838; border:2px solid #FFF;-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;}
.bottom_big_icons_logo_brounds_ins{position:absolute; width:100%;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%); text-align:center;font-family: 'awesome', sans-serif;font-size:25px; color:#FFF;-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;}

}

@media only screen and (max-width: 1440px) { 

}

@media only screen and (max-width: 1280px) { 

}

@media only screen and (max-width: 1024px) { 

}


@media only screen and (max-width: 768px){
.botton_tvkeszthely_logo{width:auto; float:none;font-family: 'Playfair Display', sans-serif;color:#FFF;font-size:28px; line-height:14px; font-style:normal; text-transform:uppercase; display:none;}
.botton_tvkeszthely_logo_left{width:100%; float:none; margin:0px 0px 0px 0px;}
.botton_tvkeszthely_logo_left li {
    float: none;
}
.botton_tvkeszthely_logo_left li a {

	margin:0px 0px 0px 0px;

}
}

@media only screen and (max-width: 479px){


}

</style>
<div class="allbottom_cont">
	<div class="allbottom_cont_ins">
    	<div class="allbottom_cont_feliart_cont">
        	<div class="botton_tvkeszthely_logo">          
            </div>
        	    <div class="botton_tvkeszthely_logo_left">
                <ul>
                    <li>
                      <a href="https://www.facebook.com/fantasticstore11" target="_blank">
                        <div style="font-size: 24px; font-family: 'awesome', sans-serif; color: #FFFFFF;">&#xf082;</div>
                      </a>
                    </li>
                    <li><a href="/gyik">Gyakori kérdések</a></li>
                    <li><a href="/szallitasi-feltetelek/">Szállítási feltételek</a></li>
                    <li><a href="/szemelyes-atvetel/">Személyes átvétel</a></li>
                    <li><a href="/vasarlasi-feltetelek/">ÁSZF, adatkezelés</a></li>
                    <li><a href="/kapcsolat">kapcsolat</a></li>                     
                </ul> 
            </div>
        </div>

        <div class="bottom_big_icons_logo_allcont">
                <div class="bottom_big_icons_logo">
                  <a href="/bariontst.php">
                    <img src="/images/barion-card-strip-intl__medium.png" style="" />
                  </a>
                </div>
        </div>        

        <div style="width: 100%; display: inline-block; margin-bottom: 20px;">
        </div>

    </div>
</div>