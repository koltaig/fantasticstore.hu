<?php
// CAPRI gyűrű file import

function magyarkarakter($str) {
	$ekezet= array("-","/","&#8482;","?","Ö","ö","Ü","ü","ű","Ó","ó","O","o","Ú","ú","Á","á","U","u","É","é","Í","í"," ","+","'","ő", "Ű", "Ő", "ä","Ä","ű","Ű","ő","Ő"," ",",","!","?",":","/","(",")","","š","ę","","č","ň","","","Č","","ô","ý","ä","ž","d'","ł","ë","â","ź","","ć","ě","ř","ń","Ł","","Ô","ň","<",">","#","@",".","„","”","č");
	$nekezet=array("","","","","O","o","U","u","u","O","o","O","o","U","u","A","a","U","u","E","e","I","i","-","-","-","o", "U", "O", "a","A","u","u","o","o","","","","","","","","","s","a","e","S","c","n","z","s","C","Z","o","y","a","z","d","L","e","a","L","t","c","e","r","n","L","s","O","n","","","","","","","","c");
	return(strtolower(str_replace($ekezet,$nekezet,$str)));
}

function sql_ins_ext_val($extvalid,$extkatid,$prodid,$extvalue) {
	$query_extval = "INSERT INTO termekek_extra_kats_values VALUES ($extvalid,$extkatid,$prodid,'$extvalue')";	
	$mq3result=mysql_query($query_extval);	
	if (!$mq3result) {
		//die('Invalid query: ' . mysql_error());
		echo "<div><b>INSERT termekek_extra_kats_values failed:</b>".$query_extval."</div>";
	} else {
		echo "<div><b>INSERT termekek_extra_kats_values successful:</b>".$query_extval."</div>";
		$extvalid++;
	}
	return ($extvalid);
}

function rem_dup_spc($str) {
	$str = trim($str);
	$str = str_replace(["\r", "\n", "\t"], " ", $str);
	while (strpos($str, "  ") !== false)
	{
		$str = str_replace("  ", " ", $str);
	}
	return ($str);
}

// CAPRI gyűrű file import
$title = "CAPTI Gyűrűk import - (SQL commits , then !!!!NOT remove csv input file)<br>";
echo "<html>\n<head>\n";
echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8" /> '."\n";
//echo '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">'."\n";
//echo '<meta http-equiv="Content-Type" content="text/html; charset=win-1250">'."\n";
echo "<style>";
echo "table,th,td {padding: 2px;border: 1px solid black;border-collapse: collapse;}";
echo "</style>";
echo "<title>$title</title>\n";
echo "</head>\n<body>\n";
echo $title;	
require ('../../adbconnect.php');

// figyelem a file a /import könyvtárból jön !NEM a feeds/import-ból a php mellett
$fn="../../import/FANTASTIC_CAPRI.csv";

//if (!setlocale(LC_ALL, "hu_HU.ISO-8859-2")){  
if (!setlocale(LC_ALL, "hu_HU.utf-8")){  
	echo("Lokalizáció beállítása nem sikerült<br>");  
  }  
  else{  
	echo(strftime("%Y.%m.%d - %A<br>"));  
  } 

echo "<table>\n";
echo "<tr><td>ROW</td><td>Cikkszam</td><td>Termek_nev</td><td>Akt_ara</td><td>jtab_kepnev</td><td>e_karat</td><td>Suly</td><td>e_kovekszam</td>";
echo "<td>e_szin</td><td>e_szelesseg</td><td>e_kulso</td><td>e_belso</td><td>e_koatmero</td><td>e_gy_karat</td><td>e_gy_csiszoltsag</td>";
echo "<td>e_gy_tisztasag</td><td>e_cirkonia</td><td>e_gyemant</td><td>e_nemkoves</td><td>e_dragakoves</td>";
//echo "<td>Leiras</td>";
echo"</tr>\n";

$rowcou = 0;
if (($handle = fopen($fn, "r")) !== FALSE) {
	echo "opening file:$fn OK.<br>";	
    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
        $num = count($data);
        $rowcou++;

		if (($data[1]!='') and (substr($data[1],0,6)!='cikksz') and ($data[1]!='product_sku')) { //ha van cikkszám és nem header row
			//convert excel hun characters to UTF-8
			for ($c=0; $c < $num; $c++) {
				$data[$c] = rem_dup_spc(iconv("ISO-8859-2","UTF-8", $data[$c]));
			}
			$cikkszam = $data[1]; 					// product_sku	Cikkszam Torzscikkszam
			// cut CAPRI if it part of Product name
			if (strtolower(substr($data[2],0,5))=='capri') {
				$data[2]=substr($data[2],6);
			}
			$Leiras = "<p>".$data[2]." </p>";			// orig name -> basic product description Leiras
			$Leiras = $Leiras."<p>".$data[29]." </p>"; 	// desc3	Leiras 	
			$Leiras = $Leiras."<p>".$data[3]." </p>";	// description	Leiras
			$Leiras = $Leiras."<p>".$data[27]." </p>";

			//OFF:4	//noi_meret								
			//OFF:5	//ferfi_meret								
			//OFF:6	//valaszthatoko								
			$Akt_ara = (int)$data[7]; 					// Price_0	Akt_ara 	Akt_ara_netto 	Orig_ara 	Orig_ara_netto 	Viszont_ara 	Viszont_ara_netto 	Besz_ar 	Besz_ar_netto
			$Orig_ara = $Akt_ara;
			$Viszont_ara = $Akt_ara;
			$Besz_ar = $Akt_ara;
			$Akt_ara_netto = round($Akt_ara/1.27);
			$Orig_ara_netto = $Akt_ara_netto;
			$Viszont_ara_netto = $Akt_ara_netto;
			$Besz_ar_netto = $Akt_ara_netto;
			//$jt_kepnev = "https://www.caprikarikagyuruk.hu/ftp/webstore_images/products/".strtolower($data[8]);
			$jt_kepnev = $data[30].strtolower($data[8]);
											// image	kepnev in TABLE 'termek_kepek' (Gal_id=termek_id, Helyezes=1, Kezdo=0)
			if ($data[9]=="x") {$User_aktiv = 0;} else {$User_aktiv = 1;} 
											//	elfogyott	User_aktiv
			//OFF:10	//elore								
			$jwl_suly = $data[12]; 				// suly	Suly 							
			$jwl_karat = $data[11]; 			// arany karat 		ext_kats_id:3000
			$jwl_kovekszam = $data[13]; 		// kovekszam 	ext_kats_id:3001
			$jwl_szin = $data[14]; 			// szin				ext_kats_id:3002
			$jwl_szelesseg = $data[15]; 		// szelesseg	ext_kats_id:3003
			$jwl_kulso = $data[16]; 			// kulso		ext_kats_id:3004
			$jwl_belso = $data[17]; 			// belso		ext_kats_id:3005
			//OFF:18	//elviheto								
			$jwl_koatmero = $data[19]; 		// koatmero			ext_kats_id:3006	
			$gem_karat = $data[20]; 		// gyemant_karat	ext_kats_id:3007
			$gem_csiszoltsag = $data[21]; 	// gyemant_csiszoltsag	ext_kats_id:3008
			$gem_tisztasag = $data[22]; 	// szin_tisztasag	ext_kats_id:3009
			$jwl_cirkonia = $data[23]; 		// cirkonia			ext_kats_id:3010
			$jwl_gyemant = $data[24]; 		// gyemant			ext_kats_id:3011
			$jwl_nemkoves = $data[25]; 		// nemkoves			ext_kats_id:3012
			$jwl_dragakoves = $data[26]; 		// dragakoves	ext_kats_id:3013			
			
			//OFF:28	//üres mező								


			// constant import filed values
			$Gyarto = 3000; 					//const ID of "Capri" in db table Gyarto 
			$Akcios = 0; 						//const: 0	Akcios
			$Kedvezmeny_percent = 0; 			//const: 0	Kedvezmeny_percent
			$date_now = date("Y-m-d H:i:s");
			$Feltoltve = "$date_now"; 			//const: $now	Feltoltve 
			$Afa = 27; 							//const: 27	Afa 
			$Penznem = "HUF"; 					//const: "HUF"	Penznem
			$Fokat_id = 45; 					//const: 45 //Ékszer	Fokat_id 
			$Db_szam = 9999;					//const: 9999	Db_szam - a termék nem fogy el a kosár gomb elérhető marad
			$Egyseg = "db"; 					//const: "db"	Egyseg 
			$Neme = 290;						//const: "Női és Férfi"
			
			//Termek_nev összeállítása

			//$Alkat_id comes from $data[0]; 					// type	Alkat_id
			//Parse alkat_id
			if ($data[0]=='karikagyűrű') {
				$Alkat_id=6000;
				$Termek_nev = "Karikagyűrű";
				if (strpos($data[2], ' PÁRBAN', 0)!=false) {			
					$Termek_nev .= " párban";
				}
			}
			if ($data[0]=='eljegyzesigyűrű') {
				$Alkat_id=6001;
				$Termek_nev = "Eljegyzési gyűrű";
			}
			if ($data[0]=='ékszer') { //ékszer esetében terméknv csvből direkt
				$Alkat_id=6002;
				$Termek_nev = $data[2];
			}			
			$nev_opt = $Termek_nev;						// name	Termek_nev nev_opt

			if (($Alkat_id==6000) OR ($Alkat_id==6001)) { //karikagyűrű vagy eljegyzési gyűrű esetében terméknv összeállítása
				if ($jwl_karat!="") {
					$Termek_nev .= " ".$jwl_karat."K arany $jwl_suly";
				}
				if ($jwl_cirkonia!='') {
					$Termek_nev .= ", cirkónia kővel";
				}
				if ($jwl_gyemant!='') {
					$Termek_nev .= ", gyémánt kővel $gem_karat";
				}
				if ($jwl_nemkoves!='') {//kő nélkül nem kerül kiírásra a terméknévben
				}
				if ($jwl_dragakoves!='') {
					if ($jwl_dragakoves=='x') {
						$Termek_nev .= ", drága kővel";	
					} else {
						$Termek_nev .= ", $jwl_dragakoves drága kővel";
					}				
				}
			}
			$Termek_nev .= ", $cikkszam";

			echo "<tr><td>$rowcou</td><td>$cikkszam</td><td>$Termek_nev</td><td>$Akt_ara</td><td><img src='$jt_kepnev' height='100'></td><td>$jwl_karat</td><td>$jwl_suly</td><td>$e_kovekszam</td>";
			echo "<td>$jwl_szin</td><td>$jwl_szelesseg</td><td>$jwl_kulso</td><td>$jwl_belso</td><td>$jwl_koatmero</td><td>$gem_karat</td><td>$gem_csiszoltsag</td>";
			echo "<td>$gem_tisztasag</td><td>$jwl_cirkonia</td><td>$jwl_gyemant</td><td>$jwl_nemkoves</td><td>$jwl_dragakoves</td>";
			//echo "<td>$Leiras</td>";
			echo"</tr>\n";
		
			echo"<tr><td colspan='100%'>\n";
			$result = mysql_query("SELECT Id FROM termekek WHERE Cikkszam = '".$cikkszam."'");
			$row = mysql_fetch_assoc($result);
			$upd_need = $row['Id'];
			if(!isset($upd_need)){//insert needed
				// termekek table Insert
				// get last termek id
				if (!isset($new_id)) {	//volt-e már max kikeresés
					$result = mysql_query("SELECT MAX(Id) AS last_id FROM termekek");
					$row = mysql_fetch_assoc($result);
					$last_id = $row['last_id'];
					if ($last_id<100000) {
						$new_id=100000;
					} else {
						$new_id=$last_id+1;
					}
				} else {
					$new_id++;
				}
				$Termek_nev_opt = magyarkarakter($Termek_nev);		
				$query = "INSERT INTO termekek VALUES ($new_id,'$cikkszam','','$cikkszam','$Termek_nev','$Termek_nev_opt',$Neme,$Gyarto,$Akt_ara,$Akt_ara_netto,$Akcios,$Orig_ara,";
				$query .= "$Orig_ara_netto,$Besz_ar,$Besz_ar_netto,$Viszont_ara,$Viszont_ara_netto,$Kedvezmeny_percent,'$Leiras','$Feltoltve',$Afa,'$Penznem',$Fokat_id,$Alkat_id,";
				$query .= "'','','','','',$User_aktiv,'','','','',$Db_szam,'0.1','$Egyseg',NULL)";	
				$mq1result=mysql_query($query);
				if (!$mq1result) {
					//die('Invalid query: ' . mysql_error());
					echo "<div><b>INSERT termekek failed:</b>".$query."</div>";
				} else {
					echo "<div><b>INSERT termekek successful:</b>".$query."</div>";
				}			
				// termek_kepek table Insert
				// get last termek_kep id
				if (!isset($kep_new_id)) {	//volt-e már max kikeresés
					$result = mysql_query("SELECT MAX(Id) AS kep_last_id FROM termek_kepek");
					$row = mysql_fetch_assoc($result);
					$kep_last_id = $row['kep_last_id'];
					if ($kep_last_id<300000) {
						$kep_new_id=300000;
					} else {
						$kep_new_id=$kep_last_id+1;
					}
				} else { //előző sor feldolgozáshoz+1
					$kep_new_id++;
				}
				$query2 = "INSERT INTO termek_kepek VALUES ($kep_new_id,1,$new_id,0,'$jt_kepnev')";	
				$mq2result=mysql_query($query2);
				if (!$mq2result) {
					//die('Invalid query: ' . mysql_error());
					echo "<div><b>INSERT termek_kepek failed:</b>".$query2."</div>";
				} else {
					echo "<div><b>INSERT termek_kepek successful:</b>".$query2."</div>";
				}

				// termekek_extra_kats_values table Insert
				// get termekek_extra_kats_values id
				if (!isset($ext_val_new_id)) {	//volt-e már max kikeresés
					$result = mysql_query("SELECT MAX(Id) AS ext_val_last_id FROM termekek_extra_kats_values");
					$row = mysql_fetch_assoc($result);
					$ext_val_last_id = $row['ext_val_last_id'];
					if ($ext_val_last_id<30000000) {
						$ext_val_new_id=30000000;
					} else {
						$ext_val_new_id=$ext_val_last_id+1;
					}
				} 
				// doing extra values SQL inserts
				if ($jwl_karat!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3000,$new_id,$jwl_karat);
				}
				if ($jwl_suly!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,31,$new_id,$jwl_suly);
				}
				if ($jwl_kovekszam!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3001,$new_id,$jwl_kovekszam);
				}
				if ($jwl_szin!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3002,$new_id,$jwl_szin);
				}
				if ($jwl_szelesseg!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3003,$new_id,$jwl_szelesseg);
				}
				if ($jwl_kulso!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3004,$new_id,$jwl_kulso);
				}
				if ($jwl_belso!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3005,$new_id,$jwl_belso);
				}
				if ($jwl_koatmero!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3006,$new_id,$jwl_koatmero);
				}
				if ($gem_karat!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3007,$new_id,$gem_karat);
				}
				if ($gem_csiszoltsag!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3008,$new_id,$gem_csiszoltsag);
				}
				if ($gem_tisztasag!='') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3009,$new_id,$gem_tisztasag);
				}
				if ($jwl_cirkonia=='x') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3010,$new_id,'igen');
				}
				if ($jwl_gyemant=='x') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3011,$new_id,'igen');
				}
				if ($jwl_nemkoves=='x') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3012,$new_id,'igen');
				}
				if ($jwl_dragakoves=='x') {
					$ext_val_new_id=sql_ins_ext_val($ext_val_new_id,3013,$new_id,'igen');
				}
		
				//insert new price history record
				/*$now = new DateTime();
				$mysqltime_now = $now->format('Y-m-d H:i:s');  
				$query_insert_phist = "INSERT INTO termekek_price_hist VALUES ('','$new_id','$Akt_ara','$Akt_ara_neto','$Feltoltve')";
				$query_insert_phist_res=mysql_query($query_insert_phist);
				if (!$query_insert_phist_res) {
					//die('Invalid query: ' . mysql_error());
					echo "<div><b>INSERT termekek_price_hist failed:</b>".$query_insert_phist."</div>";
				} else {
					echo "<div><b>INSERT termekek_price_hist successful:</b>".$query_insert_phist."</div>";
				}*/
				echo "<div style=\" border-top:0px solid #000; width:100%; text-transform: none;\">Új cikkszám: ".$cikkszam.", (INSERT)</div>";
			} else { //termekek update needed
				echo "<div style=\" border-top:0px solid #000; width:100%; text-transform: none;\">Már létező cikkszám: $cikkszam, (UPDATE ID:$upd_need)</div>";
				$query = "UPDATE termekek SET Akt_ara=$Akt_ara, Akt_ara_netto=$Akt_ara_netto, Orig_ara=$Orig_ara, Orig_ara_netto=$Orig_ara_netto, Leiras='$Leiras', Db_szam=$Db_szam WHERE Id=$upd_need";
				echo "<div style=\" border-top:0px solid #000; width:100%; text-transform: none;\">(UPDATE SQL:$query)</div>";				
				$mqresult=mysql_query($query);				
				if (!$mqresult) {
					echo "  UPDATE FAILED<br>";					
				} else {
					echo "  UPDATE SUCESSFUL<br>";										
				}

			}//termekek insert or update end
			echo"</td></tr>\n";
		}
	}
    fclose($handle);
	//unlink($fn);
}
echo "</table>\n";
echo "</body>\n</html>";
?>
