<? require('../adbconnect.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Parse termek feed from swisstime</title>

<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>

</head>

<body style="margin:0px; padding:0px;">

<?

$date = date("Y-m-d H:i:s");
$date_r = date("Y-m-d");

$separator=";";
$mit = array('&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
//'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
'<br>','<br/>','<br />','&nbsp;','&nbsp','\n',
$separator);
$mire = array(
"'","'","'","'","'","'","'","'","'","'","'","'","'","'",
'&','-','ó','é','í','ú',
'á','é','í','ő','ű',
'á','é','í','ó','ú',
'>','<','-','-','',',','D',
'á','é','í','ó','ö','ú','ü',
'ő','ű','ő','ű',
//'®','©','™','®','©','™',
' ',' ',' ',' ',' ',' ',
",");
	
$urlmit = array(
'&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
);

$urlmire = array(
"%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27",
'%26','-','o','e','i','u',
'a','e','i','o','u',
'a','e','i','o','u',
'%3E','%3C','-','-','',',','D',
'a','e','i','o','o','u','u',
'o','u','o','u',
'%C2%AE','%C2%A9','%E2%84','%C2%AE','%C2%A9','%E2%84',
);

$query="SELECT alapok.overal_discount_percent FROM alapok";
//általános discount
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
    $discount=mysql_result($result,$i,"alapok.overal_discount_percent");
    $i++;
}

// read/display csv header
$file = "https://swisstimeshop.hu/feeds/prod_pure_db.csv";
//Id;cikkszam;szin;torzscikkszam;ter_nev;ter_nev_opt;neme;ter_gyarto;akt_ara;orig_ara;viszont_ara;feltoltve;afa;penznem;fokat_id;alkat_id;subkat_id;forgalmazo;szallitas;garancia;szallitasi_dij;aktiv;admin_aktiv;torzsvasarloi_termek;szmart;szallito;db_szam;suly;egyseg;date_actual_xml_sync;leiras;
$fileData=fopen($file,'r');
$i=0;
echo "<table>";
$line = fgetcsv($fileData,0,";");

$id=$line[0];
$cikkszam=$line[1];
$szin=$line[2];
$torzscikkszam=$line[3];
$ter_nev=$line[4];
$ter_nev_opt=$line[5];
$neme=$line[6];
$ter_gyarto=$line[7];
$akt_ara=$line[8];
$orig_ara=$line[9];
$viszont_ara=$line[10];
$feltoltve=$line[11];
$afa=$line[12];
$penznem=$line[13];
$fokat_id=$line[14];
$alkat_id=$line[15];
$subkat_id=$line[16];
$forgalmazo=$line[17];
$szallitas=$line[18];
$garancia=$line[19];
$szallitasi_dij=$line[20];
$aktiv=$line[21];
$admin_aktiv=$line[22];
$torzsvasarloi_termek=$line[23];
$szmart=$line[24];
$szallito=$line[25];
$db_szam=$line[26];
$suly=$line[27];
$egyseg=$line[28];
$date_actual_xml_sync=$line[29];
$leiras=$line[30];
$leiras=iconv('ISO-8859-2', 'UTF-8', $leiras);	


$disc_ara="discounted_ar";
$sep="</td><td>";
echo "<tr><td> ($i feed) </td><td> $id $sep $aktiv $sep $cikkszam $sep $ter_gyarto $sep $ter_nev $sep $db_szam $sep $akt_ara -> $disc_ara $sep $orig_ara $sep $leiras</td></tr>";
$i++;

// read/display csv rows
$maxrow=10000;
$total_updates=0;
$total_ins=0;
while (($i<=$maxrow) and (($line = fgetcsv($fileData,0,";")) !== FALSE)) {
   //$line = fgetcsv($fileData,0,";");

   $id=(int)$line[0];
   $cikkszam=$line[1];
   $szin=$line[2];
   $torzscikkszam=$line[3];
   $ter_nev=$line[4];
   $ter_nev=iconv('ISO-8859-2', 'UTF-8', $ter_nev);	
   $ter_nev_opt=$line[5];
   $neme=(int)$line[6];
   $ter_gyarto=(int)$line[7];
   $akt_ara=(int)$line[8];
   $orig_ara=(int)$line[9];
   $viszont_ara=(int)$line[10];
   $feltoltve=$line[11];
   $afa=(int)$line[12];
   $penznem=$line[13];
   $fokat_id=(int)$line[14];
   $alkat_id=(int)$line[15];
   $subkat_id=(int)$line[16];
   $forgalmazo=(int)$line[17];
   $szallitas=$line[18];
   $garancia=$line[19];
   $szallitasi_dij=$line[20];
   $aktiv=(int)$line[21];
   $admin_aktiv=(int)$line[22];
   $torzsvasarloi_termek=$line[23];
   $szmart=$line[24];
   $szallito=(int)$line[25];
   $db_szam=(int)$line[26];
   $suly=(float)$line[27];
   $egyseg=$line[28];
   $date_actual_xml_sync=$line[29];
   $leiras=$line[30];
   $leiras=iconv('ISO-8859-2', 'UTF-8', $leiras);	
   $disc_ara=(int)($akt_ara*(100-$discount)/100);
   $afadiv=$afa/100+1;
   $disc_ara_netto=(int)($disc_ara/$afadiv);
   $akt_ara_netto=(int)($akt_ara/$afadiv);
   $orig_ara_netto=(int)($orig_ara/$afadiv);
   $viszont_ara_netto=(int)($viszont_ara/$afadiv);
   $sep="</td><td>";

   //fetch current record from local db by (id)

   $LIMIT="LIMIT 1"; //NO LIMIT (database indexing on foreign keys is magic!:)
   $query="SELECT t.id FROM termekek t WHERE (t.id!=0) AND (t.cikkszam='$cikkszam') AND (t.Id='$id') ORDER BY t.Id DESC $LIMIT";
   //echo "$query<br>";

   $result=mysql_query($query);
   $num=mysql_numrows($result);
   $k=0;
   if ($num > 0) { //update
   } else { //insert 
      echo "<tr><td> ($i feed insert) </td><td> $id $sep $aktiv $sep $cikkszam $sep $ter_gyarto $sep $ter_nev $sep $db_szam $sep $akt_ara -> $disc_ara $sep $orig_ara $sep $leiras</td></tr>";

      $termek_kedve_szazelek_alap = round($orig_ar/100,2);
      $termek_kedve_szazelek = round(100-$orig_ar/$termek_kedve_szazelek_alap,2);					
      
      //??hulyeség Id;cikkszam;szin;torzscikkszam;ter_nev;ter_nev_opt;neme;ter_gyarto;orig_ara;viszont_ara;feltoltve;afa;penznem;fokat_id;alkat_id;subkat_id;forgalmazo;szallitas;garancia;szallitasi_dij;aktiv;admin_aktiv;torzsvasarloi_termek;szmart;szallito;db_szam;suly;egyseg;date_actual_xml_sync;leiras;
      //Id;cikkszam;szin;torzscikkszam;ter_nev;ter_nev_opt;neme;ter_gyarto;   Akt_ara; Akt_ara_netto; Akcios; Orig_ara; Orig_ara_netto; Besz_ara; Besz_ara_netto; Viszont_ara; Viszont_ara_netto; Kedvezmeny_percent; $Leiras; Feltoltve; Afa; Penznem; Fokat_id; Alkat_id; Subkat_id; $Forgalmazo; Szallitas; Garancia; Szallitasi_dij; User_aktiv; Admin_aktiv; Torzsvesarloi_termek; Szmart; Szallito; Db_szam; Suly; Egyseg; date_actual_xml_sync;
      // nem ok árak: $query = "INSERT INTO termekek VALUES ('$id','$cikkszam','$szin','$torzscikkszam','$ter_nev','$ter_nev_opt','$neme','$ter_gyarto','$disc_ara','$disc_ara_netto','0','$orig_ara','$orig_ara_netto','$akt_ara','$akt_ara_netto','$viszont_ara','$viszont_ara_netto','$termek_kedve_szazelek_alap','$leiras','$feltoltve','$afa','$penznem','$fokat_id','$alkat_id','$subkat_id','$forgalmazo','$szallitas','$garancia','$szallitasi_dij','$aktiv','$admin_aktiv','$torzsvasarloi_termek','$szmart','$szallito','$db_szam','$suly','$egyseg','$date_actual_xml_sync');";	
      $query = "INSERT INTO termekek VALUES ('$id','$cikkszam','$szin','$torzscikkszam','$ter_nev','$ter_nev_opt','$neme','$ter_gyarto','$disc_ara','$disc_ara_netto','0','$disc_ara','$disc_ara_netto','$akt_ara','$akt_ara_netto','$viszont_ara','$viszont_ara_netto','$termek_kedve_szazelek_alap','$leiras','$feltoltve','$afa','$penznem','$fokat_id','$alkat_id','$subkat_id','$forgalmazo','$szallitas','$garancia','$szallitasi_dij','$aktiv','$admin_aktiv','$torzsvasarloi_termek','$szmart','$szallito','$db_szam','$suly','$egyseg','$date_actual_xml_sync');";	
      echo "<tr><td colspan='*'> $query </td></tr>";
      $mq1result=mysql_query($query);
      if (!$mq1result) {
          //die('Invalid query: ' . mysql_error());
          echo "<div><b>INSERT termekek failed:</b>".$query."</div>";
      } else {
          echo "<div><b>INSERT termekek successful:</b>".$query."</div>";
          $total_ins++;
      }

   }
   //next row from input
   $i++;
}
echo "<tr><td> TOTAL UPDATES: ($total_updates) </td></tr>";
echo "<tr><td> TOTAL INSERTS: ($total_ins) </td></tr>";
echo "</table>";
fclose($file);

?> 

</body>
</html>