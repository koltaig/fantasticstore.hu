<? require('../adbconnect.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Parse termek feed from swisstime</title>

<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>

</head>

<body style="margin:0px; padding:0px;">

<?

$ekezet_off_mit = array(
   "Ö","Ü","Ó","Ő","Ú","Ű","Á","Í","É","Ő","Ű",
   "ö","ü","ó","ő","ú","ű","á","í","é","ő","ű",
   ", ","; "," ","(",")","'");
$ekezet_off_mire = array(
   "o","u","o","o","u","u","a","i","e","o","u",
   "o","u","o","o","u","u","a","i","e","o","u",
   "-","-","-","-","-","-");

$separator=";";
$mit = array('&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
//'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
'<br>','<br/>','<br />','&nbsp;','&nbsp','\n',
$separator);
$mire = array(
"'","'","'","'","'","'","'","'","'","'","'","'","'","'",
'&','-','ó','é','í','ú',
'á','é','í','ő','ű',
'á','é','í','ó','ú',
'>','<','-','-','',',','D',
'á','é','í','ó','ö','ú','ü',
'ő','ű','ő','ű',
//'®','©','™','®','©','™',
' ',' ',' ',' ',' ',' ',
",");
	
$urlmit = array(
'&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
);

$urlmire = array(
"%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27",
'%26','-','o','e','i','u',
'a','e','i','o','u',
'a','e','i','o','u',
'%3E','%3C','-','-','',',','D',
'a','e','i','o','o','u','u',
'o','u','o','u',
'%C2%AE','%C2%A9','%E2%84','%C2%AE','%C2%A9','%E2%84',
);

$query="SELECT alapok.overal_discount_percent FROM alapok";
//általános discount
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
    $discount=mysql_result($result,$i,"alapok.overal_discount_percent");
    $i++;
}

//gyarto db table array-be töltése
$gyarto_arr=array();
$result = mysql_query("SELECT * FROM gyarto");
//echo "gyarto_arr feltoltes start:<br>";
while ($row = mysql_fetch_assoc($result)) {
  $gy_id=trim($row["Id"]);
  $gy_nev=trim($row["Nev"]);
//  echo "($gy_id),($gy_nev)<br>";
  $gyarto_arr[$gy_id]=$gy_nev;
}
/*echo "gyarto_arr feltoltes end.<br>";
echo "gyarto_arr:<br>";
print_r($gyarto_arr);
echo "gyarto_arr end.<br>";
foreach (array_keys($gyarto_arr) as $key) {
   $value = $gyarto_arr[$key];
   echo "($key,$value)<br>";
 }*/

//alkat db table array-be töltése
$alkat_arr=array();
$result = mysql_query("SELECT * FROM alkategoriak");
//echo "alkat_arr feltoltes start:<br>";
while ($row = mysql_fetch_assoc($result)) {
  $alk_id=trim($row["Id"]);
  $alk_nev=trim($row["Nev"]);
  //echo "($alk_id),($alk_nev)<br>";
  $alkat_arr[$alk_id]=$alk_nev;
}

//fokat db table array-be töltése
$fokat_arr=array();
$result = mysql_query("SELECT * FROM fokategoriak");
//echo "fokat_arr feltoltes start:<br>";
while ($row = mysql_fetch_assoc($result)) {
  $fok_id=trim($row["Id"]);
  $fok_nev=trim($row["Nev"]);
  //echo "($fok_id),($fok_nev)<br>";
  $fokat_arr[$fok_id]=$fok_nev;
}

//neme db table array-be töltése
$neme_arr=array();
$result = mysql_query("SELECT * FROM nemek");
//echo "nemek_arr feltoltes start:<br>";
while ($row = mysql_fetch_assoc($result)) {
  $neme_id=trim($row["Id"]);
  $neme_nev=trim($row["Nev"]);
  //echo "($neme_id),($neme_nev)<br>";
  $neme_arr[$neme_id]=$neme_nev;
}

// read/display csv header
$file = "https://swisstimeshop.hu/feeds/full_prod_fantasticst.csv";
$fileData=fopen($file,'r');
$i=0;
echo "<table>";
$line = fgetcsv($fileData,0,";");
$id=$line[0];
$cikkszam=$line[1];
$aktiv=$line[2];
$megnevezes=$line[3];
$megnevezes=iconv('ISO-8859-2', 'UTF-8', $megnevezes);	
$link=$line[4];
$kep_link=$line[5];
$akt_ar=$line[6];
$viszonteladoi_ar=$line[7];
$orig_ar=$line[8];
$penznem=$line[9];
$keszlet=$line[10];
$mennyisegi_egys=$line[11];
$suly=$line[12];
$gyarto_nev=$line[13];
$fokategoria=$line[14];
$alkategoria=$line[15];
$termek_leiras=$line[16];
$termek_leiras=iconv('ISO-8859-2', 'UTF-8', $termek_leiras);	
$neme=$line[17];
$discounted_ar="discounted_ar";
$sep="</td><td>";
echo "<tr><td> ($i feed) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev $sep $megnevezes $sep $keszlet $sep $akt_ar -> $discounted_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
$i++;

// read/display csv rows
$maxrow=15000;
$total_updates=0;
$total_kt_updates=0;
$total_gyarto_updates=0;
$total_no_gyarto_inaktiv_updates=0;
$total_aktiv_updates=0;
$total_neme_updates=0;
$total_alkat_updates=0;
$total_fokat_updates=0;
$total_megnevezes_updates=0;

//$debug=true;
//if ($debug) { $maxrow=21; }
while (($i<=$maxrow) and (($line = fgetcsv($fileData,0,";")) !== FALSE)) {
   //$line = fgetcsv($fileData,0,";");

   $id=$line[0];
   $cikkszam=$line[1];
   $aktiv=$line[2];
   $megnevezes=$line[3];
   $megnevezes=iconv('ISO-8859-2', 'UTF-8', $megnevezes);	
   $link=$line[4];
   $kep_link=$line[5];
   $akt_ar=(int)$line[6];
   $viszont_ar=(int)$line[7];
   $orig_ar=(int)$line[8];
   $penznem=$line[9];
   $keszlet=(int)$line[10];
   $mennyisegi_egys=$line[11];
   $suly=$line[12];
   $gyarto_nev=$line[13];
   $gyarto_nev=trim(iconv('ISO-8859-2', 'UTF-8', $gyarto_nev));
   $fokategoria=trim($line[14]);
   $fokategoria=iconv('ISO-8859-2', 'UTF-8', $fokategoria);	
   $alkategoria=trim($line[15]);
   $alkategoria=iconv('ISO-8859-2', 'UTF-8', $alkategoria);	
   $termek_leiras=$line[16];
   $termek_leiras=iconv('ISO-8859-2', 'UTF-8', $termek_leiras);	
   $neme=trim($line[17]);
   $neme=iconv('ISO-8859-2', 'UTF-8', $neme);
   if ($neme == '') {
      $neme='Nincs neme';
   }
   $disc_ar=(int)($akt_ar*(100-$discount)/100);
   $sep="</td><td>";

   //fetch current record from local db by (id/cikkszam)

   $LIMIT="LIMIT 1"; //NO LIMIT (database indexing on foreign keys is magic!:)
   //$query="SELECT t.*, k.Kepnev, k.Gal_id, k.Helyezes, gy.Nev Gyarto_nev, gy.nev_opt Gyarto_nev_opt, fk.Nev Fokat_nev, ak.Nev Alkat_nev, ak.nev_opt Alkat_nev_opt FROM termekek t, termek_kepek k, gyarto gy, fokategoriak fk, alkategoriak ak WHERE (k.Gal_id=t.id) AND (k.helyezes=1) AND (gy.Id = t.Gyarto) AND (fk.Id=t.Fokat_id) AND (ak.Id=t.Alkat_id) AND (t.Id!=0) AND (gy.Feed_products=1) AND (t.cikkszam='$cikkszam') AND (t.Id='$id') ORDER BY t.Id DESC $LIMIT";
   $query="SELECT t.* FROM termekek t WHERE (t.cikkszam='$cikkszam') AND (t.Id='$id') ORDER BY t.Id DESC $LIMIT";
   if ($debug) { echo "$query<br>"; }

   $result=mysql_query($query);
   $num=mysql_numrows($result);
   //echo"num:($num)<br>";
   $k=0;
   if ($num > 0) { //ha létezik db record ID és cikkszam egyezéssel akkor update vizsgálat
      $l_id=mysql_result($result,$k,"t.Id");
      $l_cikkszam=mysql_result($result,$k,"t.Cikkszam");
      $l_aktiv=mysql_result($result,$k,"t.User_aktiv");

      $l_akt_ar=(int)floatval(mysql_result($result,$k,"t.Akt_ara")); 
      $l_orig_ar=(int)floatval(mysql_result($result,$k,"t.Orig_ara"));
      //akciós árak nem mennek ki, de van aki ebből az oszlopból olvassa a feedet 
      if ($l_akt_ar != $l_orig_ar) { //akciós a termék
         $l_akcios = '-%-';
         //$l_akt_ar=$l_orig_ar; //akciós árak helyett orig ár
      } else {
         $l_akcios = '';
      }

      $l_megnevezes=mysql_result($result,$k,"t.Termek_nev");
      
      $l_megnevezes=ucfirst(str_replace($mit, $mire, $l_megnevezes));

      $l_megnevezes_opt=mysql_result($result,$k,"t.nev_opt");

      $l_gyarto=floatval(mysql_result($result,$k,"t.Gyarto"));
      
      $l_fokat_id=floatval(mysql_result($result,$k,"t.Fokat_id"));
      $l_alkat_id=floatval(mysql_result($result,$k,"t.Alkat_id"));
      $l_neme=floatval(mysql_result($result,$k,"t.Neme"));
      
      $l_leiras=mysql_result($result,$i,"t.Leiras");
      $l_leiras=str_ireplace($mit, $mire, $l_leiras);
      $l_leiras=strip_tags($l_leiras);
      $l_leiras = str_replace("\r\n","",$l_leiras);	
      $l_leiras = preg_replace('~[[:cntrl:]]~', '', $l_leiras); // remove all control chars

      $l_penznem=mysql_result($result,$k,"t.Penznem");
      $l_suly=mysql_result($result,$k,"t.Suly");
      $l_keszlet=mysql_result($result,$k,"t.Db_szam");

      $l_egyseg=mysql_result($result,$k,"t.Egyseg");

      $l_kedv = mysql_result($result,$k,"t.Kedvezmeny_percent");	

/*      $l_gyarto_nev = mysql_result($result,$k,"Gyarto_nev");
      $l_alkat_nev = mysql_result($result,$k,"Alkat_nev");	
      $l_fokat_nev = mysql_result($result,$k,"Fokat_nev");	*/
      $l_alkat_nev = $alkat_arr[$l_alkat_id];
      $l_fokat_nev = $fokat_arr[$l_fokat_id];
      $l_neme_nev = $neme_arr[$l_neme];
      

      $upd_need=0;
      $upd_need_aktiv=0;
      $upd_need_megnevezes=0;
      $upd_need_gyarto=0;
      $upd_need_inakt_no_gyarto=0;
      $upd_need_neme=0;
      $upd_need_alkat=0;
      $upd_need_fokat=0;

      $upd_str='';
      $kt_upd_str='';
      if ($debug) { echo "aktiv: DB($l_aktiv) CSV($aktiv)<br>"; }
      if ($l_aktiv != $aktiv) {
         if ($upd_need>0) {
            $upd_str.=",";
         }
         $upd_need++;
         $upd_need_aktiv++;
         $total_aktiv_updates++;
         $upd_str.="User_aktiv=$aktiv";
      }

      if ($debug) { echo "megnevezes: DB($l_megnevezes) CSV($megnevezes)<br>"; }
      if ($l_megnevezes != $megnevezes) {
         if ($upd_need>0) {
            $upd_str.=",";
         }
         $upd_need++;
         $upd_need_megnevezes++;
         $total_megnevezes_updates++;
         $upd_str.='Termek_nev="'.$megnevezes.'"';     
         $megnevezes_opt= str_replace($ekezet_off_mit, $ekezet_off_mire, $megnevezes);
         $megnevezes_opt = strtolower($megnevezes_opt);
         $upd_str.=",nev_opt='$megnevezes_opt'";
         echo "<tr><td>AAA MEGN: ($l_megnevezes -> $megnevezes) update str: ($upd_str)</td></tr>";
      }

      if ($debug) { echo "keszlet: DB($l_keszlet) CSV($keszlet)<br>"; }
      if ($l_keszlet != $keszlet) {
         if ($upd_need>0) {
            $upd_str.=",";
         }
         $upd_need++;
         $upd_str.="Db_szam='$keszlet'";
      }

      //echo "<tr><td>update need: $upd_need update str: ($upd_str)</td></tr>";
      $gyarto_id=array_search($gyarto_nev,$gyarto_arr); //gyarto key search gyarton_nev csv field alapján
      //echo "($gyarto_nev) kereses. gyarto_arr-ben.<br>";
      if ($gyarto_id==false) { //nincs gyarto
         //echo "<tr><td> ($i feed update:$upd_need xxx no_gyarto) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev / $l_gyarto -> 0 $sep $megnevezes $sep $keszlet $sep $akt_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
         if ($upd_need>0) {
            $upd_str.=",";
         }
         $upd_str.="User_aktiv=0";
         $upd_need++;
         $upd_need_inakt_no_gyarto++;
         $total_no_gyarto_inaktiv_updates++;
         //echo "($gyarto_nev) kereses. NINCS gyarto_arr-ben.<br>";
      } else { //talált gyarto key
         //echo "($gyarto_nev) kereses. MEGVAN gyarto_arr-ben. ($gyarto_id) kulcs-al<br>";
         if ($l_gyarto!=$gyarto_id){ //megvan a gyártó kódja, de nem ez van a db-ban
            //echo "($gyarto_id) nem egyezik db tárolt gyarto kulcsal ($l_gyarto)<br>";            
            if ($upd_need>0) {
               $upd_str.=",";
            }
            $upd_need++;
            $upd_str.="gyarto=$gyarto_id";
            $upd_need_gyarto++;
            $total_gyarto_updates++;
        }
      }

      if ($l_neme != $neme) {
         echo "neme check($id / db:$l_neme / csv:$neme)<br>";
         if ($upd_need>0) {
            $upd_str.=",";
         }
         $upd_str.="Neme=$neme";            
         $upd_need++;
         $upd_need_neme++;
         $total_neme_updates++;
      }

      if ($l_alkat_nev != $alkategoria) {
         echo "alkat check($id / db:$l_alkat_nev / csv:$alkategoria)<br>";
         $alkat_id=array_search($alkategoria,$alkat_arr); 
         if ($alkat_id==false) { //nincs meg az alkategória
         } else {
            if ($upd_need>0) {
               $upd_str.=",";
            }
            $upd_str.="Alkat_id=$alkat_id";            
            $upd_need++;
            $upd_need_alkat++;
            $total_alkat_updates++;
         }
      }

      if ($l_fokat_nev != $fokategoria) {
         echo "fokat check($id / db:$l_fokat_nev / csv:$fokategoria)<br>";
         $fokat_id=array_search($fokategoria,$fokat_arr); 
         if ($fokat_id==false) { //nincs meg az fokategória
         } else {
            if ($upd_need>0) {
               $upd_str.=",";
            }
            $upd_str.="Fokat_id=$fokat_id";            
            $upd_need++;
            $upd_need_fokat++;
            $total_fokat_updates++;
         }
      }


      if ($l_orig_ar != $disc_ar) {

         if ($l_akcios!='') {//akcios akt ar és kampany termkekek
            // kampany_termekek: (Ter_id=l_id) 	Akt_ara=orig_ar 	Akt_ara_netto=orig_ar/1,27 	Akcios=0 	Orig_ara=orig_ar 	Orig_ara_netto=orig_ar/1,27 	Kedvezveny=0
            // termek:           (Id=l_id) 	Akt_ara=(orig_ar*(100-l_kedv)/100) 	Akt_ara_netto=(orig_ar*(100-l_kedv)/100/1,27) 	Akcios=1 	Orig_ara=orig_ar 	Orig_ara_netto=orig_ar/1,27 	Viszont_ara=orig_ar*0,7 	Viszont_ara_netto=orig_ar*0,7/1,27
            
            //termekek
            if ($upd_need>0) {
               $upd_str.=",";
            }
            //akcios termek akt_ar
               $tmp_ar=(int)($disc_ar*(100-$l_kedv)/100);
               $upd_str.="Akt_ara=$tmp_ar";
               $tmp_ar=(int)($tmp_ar/1.27);
               $upd_str.=",Akt_ara_netto=$tmp_ar";
               //akcios termek akcio (nem kell, mert eleve ez van a dbben)
               //$upd_str.=",Akcios='1'";
               //akcios termek orig_ar
               $upd_str.=",Orig_ara=$disc_ar";
               $tmp_ar=(int)($disc_ar/1.27);
               $upd_str.=",Orig_ara_netto=$tmp_ar";
               //akcios termek viszont_ar
               $tmp_ar=(int)($orig_ar*0.7);
               $upd_str.=",Viszont_ara=$tmp_ar";
               $tmp_ar=(int)($tmp_ar/1.27);
               $upd_str.=",Viszont_ara_netto=$tmp_ar";

            //kampany_termekek (akcios)
               $kt_upd_str.="Akt_ara=$disc_ar";
               $kt_upd_str.=",Orig_ara=$disc_ar";
               $tmp_ar=(int)($disc_ar/1.27);
               $kt_upd_str.=",Akt_ara_netto=$tmp_ar";
               $kt_upd_str.=",Orig_ara_netto=$tmp_ar";
               //(nem kell, mert eleve ez van a dbben)
               //$kt_upd_str.=",Akcios='0'";
         } else { //nem akcios
            if ($upd_need>0) {
               $upd_str.=",";
            }
            //nem akcios termek akt_ar
               $upd_str.="Akt_ara=$disc_ar";
               $tmp_ar=(int)($disc_ar/1.27);
               $upd_str.=",Akt_ara_netto=$tmp_ar";
               //nem akcios termek orig_ar
               $upd_str.=",Orig_ara=$disc_ar";
               $upd_str.=",Orig_ara_netto=$tmp_ar";
               $tmp_ar=(int)($orig_ar*0.7);
               //nem akcios termek viszont_ar
               $upd_str.=",Viszont_ara=$tmp_ar";
               $tmp_ar=(int)($tmp_ar/1.27);
               $upd_str.=",Viszont_ara_netto=$tmp_ar";
         }
         $upd_need++;
      }


      $k++; 
      if ($upd_need>0) { //update needed       
         $total_updates++;
         // termek:           (Id=l_id) 	Termek_nev 	nev_opt 	Neme 	Gyarto 	Akt_ara 	Akt_ara_netto 	Akcios 	Orig_ara 	Orig_ara_netto 	Besz_ar 	Besz_ar_netto 	Viszont_ara 	Viszont_ara_netto 	Kedvezmeny_percent
         //UPDATE `table_name` SET `column_name` = `new_value' [WHERE condition];
         if ($upd_need>0) {
            echo "<tr><td> ($i feed update:$upd_need) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto $sep $megnevezes $sep $keszlet $sep $akt_ar -> $disc_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
            echo "<tr><td> ($k $l_akcios db) </td><td> $l_id $sep $l_aktiv $sep $l_cikkszam $sep $l_gyarto $sep $l_megnevezes $sep $l_keszlet $sep $l_akt_ar $sep $l_orig_ar $sep $termek_leiras</td></tr>";  
            echo "<tr><td> ($i feed price compare) db-orig:$l_orig_ar feed-disc:$disc_ar</td></tr>";
            $upd_str="UPDATE termekek SET $upd_str WHERE Id=$l_id";
            echo "<tr><td> UPDATE1: ($upd_str) </td></tr>";
            $upd_need++;
            if(mysql_query($upd_str)){
            } else {
               echo "<tr><td>ERROR:".mysql_error()."</td></tr>";
            }
            if ($kt_upd_str!='') {
               $total_kt_updates++;
               $kt_upd_str="UPDATE kampany_termekek SET $kt_upd_str WHERE Ter_id=$l_id";
               echo "<tr><td> UPDATE2: ($kt_upd_str) </td></tr>";
               if(mysql_query($kt_upd_str)) {
               } else {
                  echo "<tr><td>ERROR:".mysql_error()."</td></tr>";
               }
            }
         }
      }
   } else { //insert 
      //echo "<tr><td> ($i feed insert) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev $sep $megnevezes $sep $keszlet $sep $akt_ar -> $disc_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
   }
   //next row from input
   $i++;
}
echo "<tr><td> TOTAL UPDATES: ($total_updates) </td></tr>";
echo "<tr><td> TOTAL kampany_termekek UPDATES: ($total_kt_updates) </td></tr>";
echo "<tr><td> TOTAL gyarto UPDATES: ($total_gyarto_updates) </td></tr>";
echo "<tr><td> TOTAL no_gyarto_inaktiv UPDATES: ($total_no_gyarto_inaktiv_updates) </td></tr>";
echo "<tr><td> TOTAL User_aktiv UPDATES: ($total_aktiv_updates) </td></tr>";
echo "<tr><td> TOTAL Termek nev/opt UPDATES: ($total_megnevezes_updates) </td></tr>";
echo "<tr><td> TOTAL Neme UPDATES: ($total_neme_updates) </td></tr>";
echo "<tr><td> TOTAL Alkat_id UPDATES: ($total_alkat_updates) </td></tr>";
echo "<tr><td> TOTAL Fokat_id UPDATES: ($total_fokat_updates) </td></tr>";

echo "</table>";

fclose($file);

//echo"gyarto_arr:<br>";
//print_r($gyarto_arr);

?> 

</body>
</html>