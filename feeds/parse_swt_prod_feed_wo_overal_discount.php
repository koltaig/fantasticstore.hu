<? require('../adbconnect.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Parse termek feed from swisstime</title>

<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>

</head>

<body style="margin:0px; padding:0px;">

<?

$separator=";";
$mit = array('&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
//'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
'<br>','<br/>','<br />','&nbsp;','&nbsp','\n',
$separator);
$mire = array(
"'","'","'","'","'","'","'","'","'","'","'","'","'","'",
'&','-','ó','é','í','ú',
'á','é','í','ő','ű',
'á','é','í','ó','ú',
'>','<','-','-','',',','D',
'á','é','í','ó','ö','ú','ü',
'ő','ű','ő','ű',
//'®','©','™','®','©','™',
' ',' ',' ',' ',' ',' ',
",");
	
$urlmit = array(
'&quot;','&apos;','&ldquo;','&sbquo;','&lsquo;','&ldquo;','&bdquo;','&rdquo;','&ldquo;','&rsquo;','&uml;','&laquot;','&raquot;','&acute;',
'&amp;','&ndsah;','&ocirc;','&ecirc;','&icirc;','&ucirc;',
'&atilde;','&etilde;','&itilde;','&otilde;','&utilde;',
'&agrave;','&egrave;','&igrave;','&ograve;','&ugrave;',
'&gt;','&lt;','&hellip;','&marc;','&deg;','&cedil;','&#216;',
'&aacute;','&eacute;','&iacute;','&oacute;','&ouml;','&uacute;','&uuml;',
'&#337;','&#369;','&#336;','Ű',
'&reg;','&copy;','&trade;','&#174;','&#169;','&#8482;',
);

$urlmire = array(
"%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27","%27",
'%26','-','o','e','i','u',
'a','e','i','o','u',
'a','e','i','o','u',
'%3E','%3C','-','-','',',','D',
'a','e','i','o','o','u','u',
'o','u','o','u',
'%C2%AE','%C2%A9','%E2%84','%C2%AE','%C2%A9','%E2%84',
);

$query="SELECT alapok.overal_discount_percent FROM alapok";
//általános discount
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
    $discount=mysql_result($result,$i,"alapok.overal_discount_percent");
    $i++;
}

// read/display csv header
$file = "https://swisstimeshop.hu/feeds/full_prod_stock_price.csv";
$fileData=fopen($file,'r');
$i=0;
echo "<table>";
$line = fgetcsv($fileData,0,";");
$id=$line[0];
$cikkszam=$line[1];
$aktiv=$line[2];
$megnevezes=$line[3];
$megnevezes=iconv('ISO-8859-2', 'UTF-8', $megnevezes);	
$link=$line[4];
$kep_link=$line[5];
$akt_ar=$line[6];
$viszonteladoi_ar=$line[7];
$orig_ar=$line[8];
$penznem=$line[9];
$keszlet=$line[10];
$mennyisegi_egys=$line[11];
$suly=$line[12];
$gyarto_nev=$line[13];
$fokategoria=$line[14];
$alkategoria=$line[15];
$termek_leiras=$line[16];
$termek_leiras=iconv('ISO-8859-2', 'UTF-8', $termek_leiras);	
$discounted_ar="discounted_ar";
$sep="</td><td>";
echo "<tr><td> ($i feed) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev $sep $megnevezes $sep $keszlet $sep $akt_ar -> $discounted_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
$i++;

// read/display csv rows
$maxrow=10000;
$total_updates=0;
$total_kt_updates=0;
while (($i<=$maxrow) and (($line = fgetcsv($fileData,0,";")) !== FALSE)) {
   //$line = fgetcsv($fileData,0,";");

   $id=$line[0];
   $cikkszam=$line[1];
   $aktiv=$line[2];
   $megnevezes=$line[3];
   $megnevezes=iconv('ISO-8859-2', 'UTF-8', $megnevezes);	
   $link=$line[4];
   $kep_link=$line[5];
   $akt_ar=(int)$line[6];
   $viszont_ar=(int)$line[7];
   $orig_ar=(int)$line[8];
   $penznem=$line[9];
   $keszlet=(int)$line[10];
   $mennyisegi_egys=$line[11];
   $suly=$line[12];
   $gyarto_nev=$line[13];
   $gyarto_nev=iconv('ISO-8859-2', 'UTF-8', $gyarto_nev);
   $fokategoria=$line[14];
   $alkategoria=$line[15];
   $termek_leiras=$line[16];
   $termek_leiras=iconv('ISO-8859-2', 'UTF-8', $termek_leiras);	
   $disc_ar=(int)($akt_ar*(100-$discount)/100);
   $sep="</td><td>";

   //fetch current record from local db by (id)

   $LIMIT="LIMIT 1"; //NO LIMIT (database indexing on foreign keys is magic!:)
   $query="SELECT t.*, k.Kepnev, k.Gal_id, k.Helyezes, gy.Nev Gyarto_nev, gy.nev_opt Gyarto_nev_opt, fk.Nev Fokat_nev, ak.Nev Alkat_nev, ak.nev_opt Alkat_nev_opt FROM termekek t, termek_kepek k, gyarto gy, fokategoriak fk, alkategoriak ak WHERE (k.Gal_id=t.id) AND (k.helyezes=1) AND (gy.Id = t.Gyarto) AND (fk.Id=t.Fokat_id) AND (ak.Id=t.Alkat_id) AND (t.Id!=0) AND (gy.Feed_products=1) AND (t.cikkszam='$cikkszam') AND (t.Id='$id') ORDER BY t.Id DESC $LIMIT";
   //echo "$query<br>";

   $result=mysql_query($query);
   $num=mysql_numrows($result);
   $k=0;
   if ($num > 0) { //update
      $l_id=mysql_result($result,$k,"t.Id");
      $l_cikkszam=mysql_result($result,$k,"t.Cikkszam");
      $l_aktiv=mysql_result($result,$k,"t.User_aktiv");

      $l_akt_ar=floatval(mysql_result($result,$k,"t.Akt_ara")); 
      $l_orig_ar=floatval(mysql_result($result,$k,"t.Orig_ara"));
      //akciós árak nem mennek ki, de van aki ebből az oszlopból olvassa a feedet 
      if ($l_akt_ar != $l_orig_ar) { //akciós a termék
         $l_akcios = '-%-';
         //$l_akt_ar=$l_orig_ar; //akciós árak helyett orig ár
      } else {
         $l_akcios = '';
      }

      $l_megnevezes=mysql_result($result,$k,"t.Termek_nev");
      
      $l_megnevezes=ucfirst(str_replace($mit, $mire, $l_megnevezes));

      $l_megnevezes_opt=mysql_result($result,$k,"t.nev_opt");

      $l_gyarto=floatval(mysql_result($result,$k,"t.Gyarto"));
      
      $l_fokat_id=floatval(mysql_result($result,$k,"t.Fokat_id"));
      $l_alkat_id=floatval(mysql_result($result,$k,"t.Alkat_id"));
      
      $l_leiras=mysql_result($result,$i,"t.Leiras");
      $l_leiras=str_ireplace($mit, $mire, $l_leiras);
      $l_leiras=strip_tags($l_leiras);
      $l_leiras = str_replace("\r\n","",$l_leiras);	
      $l_leiras = preg_replace('~[[:cntrl:]]~', '', $l_leiras); // remove all control chars

      $l_penznem=mysql_result($result,$k,"t.Penznem");
      $l_suly=mysql_result($result,$k,"t.Suly");
      $l_keszlet=mysql_result($result,$k,"t.Db_szam");

      $l_egyseg=mysql_result($result,$k,"t.Egyseg");

      $l_gyarto_nev = mysql_result($result,$k,"Gyarto_nev");
      $l_gyarto_nev_opt = mysql_result($result,$k,"Gyarto_nev_opt");

      $l_alkat_nev = mysql_result($result,$k,"Alkat_nev");	
      $l_fokat_nev = mysql_result($result,$k,"Fokat_nev");	
      $l_kedv = mysql_result($result,$k,"Kedvezmeny_percent");	

      //    $l_alkat_nev_opt = mysql_result($result,$k,"Alkat_nev_opt");	

      $l_filename_photo_hp = "https://www.swisstimeshop.hu/termek_pics/".mysql_result($result,$k,"Kepnev")."";
      $upd_need=0;
      $upd_str='';
      $kt_upd_str='';
      if ($l_aktiv != $aktiv) {
         $upd_need++;
         if ($upd_need>1) {
            $upd_str.=",";
         }
         $upd_str.="User_aktiv='$aktiv'";
      }
      if ($l_keszlet != $keszlet) {
         $upd_need++;
         if ($upd_need>1) {
            $upd_str.=",";
         }
         $upd_str.="Db_szam='$keszlet'";
      }
      if ($l_orig_ar != $orig_ar) {
         $upd_need++;
         if ($l_akcios!='') {//akcios akt ar és kampany termkekek
            // kampany_termekek: (Ter_id=l_id) 	Akt_ara=orig_ar 	Akt_ara_netto=orig_ar/1,27 	Akcios=0 	Orig_ara=orig_ar 	Orig_ara_netto=orig_ar/1,27 	Kedvezveny=0
            // termek:           (Id=l_id) 	Akt_ara=(orig_ar*(100-l_kedv)/100) 	Akt_ara_netto=(orig_ar*(100-l_kedv)/100/1,27) 	Akcios=1 	Orig_ara=orig_ar 	Orig_ara_netto=orig_ar/1,27 	Viszont_ara=orig_ar*0,7 	Viszont_ara_netto=orig_ar*0,7/1,27
            
            //termekek
            if ($upd_need>1) {
               $upd_str.=",";
            }
            $tmp_ar=(int)($orig_ar*(100-$l_kedv)/100);
            $upd_str.="Akt_ara=$tmp_ar";
            $tmp_ar=(int)($tmp_ar/1.27);
            $upd_str.=",Akt_ara_netto=$tmp_ar";
            $upd_str.=",Akcios='1'";
            $tmp_ar=(int)($orig_ar);
            $upd_str.=",Orig_ara=$tmp_ar";
            $tmp_ar=(int)($orig_ar/1.27);
            $upd_str.=",Orig_ara_netto=$tmp_ar";
            $tmp_ar=(int)($orig_ar*0.7);
            $upd_str.=",Viszont_ara=$tmp_ar";
            $tmp_ar=(int)($tmp_ar/1.27);
            $upd_str.=",Viszont_ara_netto=$tmp_ar";

            //kampany_termekek
            $kt_upd_str.="Akt_ara=$orig_ar";
            $kt_upd_str.=",Orig_ara=$orig_ar";
            $tmp_ar=(int)($orig_ar/1.27);
            $kt_upd_str.=",Akt_ara_netto=$tmp_ar";
            $kt_upd_str.=",Orig_ara_netto=$tmp_ar";
            $kt_upd_str.=",Akcios='0'";


         } else { //nem akcios
            if ($upd_need>1) {
               $upd_str.=",";
            }
            $upd_str.="Akt_ara=$orig_ar";
            $tmp_ar=(int)($orig_ar/1.27);
            $upd_str.=",Akt_ara_netto=$tmp_ar";
            $upd_str.=",Orig_ara=$orig_ar";
            $upd_str.=",Orig_ara_netto=$tmp_ar";
            $tmp_ar=(int)($orig_ar*0.7);
            $upd_str.=",Viszont_ara=$tmp_ar";
            $tmp_ar=(int)($tmp_ar/1.27);
            $upd_str.=",Viszont_ara_netto=$tmp_ar";
         }
      }
      /*
      if ($l_gyarto_nev != $gyarto_nev) {
         $upd_need++;
      }
      if ($l_megnevezes != $megnevezes) {
         $upd_need++;
         $upd_str="Termek_nev='$megnevezes'";
      }
      */


      $k++; 
      if ($upd_need>0) { //update needed       
         $total_updates++;
         // termek:           (Id=l_id) 	Termek_nev 	nev_opt 	Neme 	Gyarto 	Akt_ara 	Akt_ara_netto 	Akcios 	Orig_ara 	Orig_ara_netto 	Besz_ar 	Besz_ar_netto 	Viszont_ara 	Viszont_ara_netto 	Kedvezmeny_percent
         //UPDATE `table_name` SET `column_name` = `new_value' [WHERE condition];
         echo "<tr><td> ($i feed update:$upd_need) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev $sep $megnevezes $sep $keszlet $sep $akt_ar -> $disc_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
         echo "<tr><td> ($k $l_akcios db) </td><td> $l_id $sep $l_aktiv $sep $l_cikkszam $sep $l_gyarto_nev $sep $l_megnevezes $sep $l_keszlet $sep $l_akt_ar $sep $l_orig_ar $sep $termek_leiras</td></tr>";  
         if ($upd_need>0) {
            $upd_str="UPDATE termekek SET $upd_str WHERE Id=$l_id";
            echo "<tr><td> UPDATE1: ($upd_str) </td></tr>";
            if(mysql_query($upd_str)){
            } else {
               echo "<tr><td>ERROR:".mysql_error()."</td></tr>";
            }
            if ($kt_upd_str!='') {
               $total_kt_updates++;
               $kt_upd_str="UPDATE Kampany_termekek SET $kt_upd_str WHERE Ter_id=$l_id";
               echo "<tr><td> UPDATE2: ($kt_upd_str) </td></tr>";
               if(mysql_query($kt_upd_str)){
               } else {
                  echo "<tr><td>ERROR:".mysql_error()."</td></tr>";
               }
            }
         }
      }
   } else { //insert 
      //echo "<tr><td> ($i feed insert) </td><td> $id $sep $aktiv $sep $cikkszam $sep $gyarto_nev $sep $megnevezes $sep $keszlet $sep $akt_ar -> $disc_ar $sep $orig_ar $sep $termek_leiras</td></tr>";
   }
   //next row from input
   $i++;
}
echo "<tr><td> TOTAL UPDATES: ($total_updates) </td></tr>";
echo "<tr><td> TOTAL kampany_termekek UPDATES: ($total_kt_updates) </td></tr>";
echo "</table>";
fclose($file);

?> 

</body>
</html>