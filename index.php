<?
ini_set('session.cache_limiter','public');
session_cache_limiter(false); 
session_start();

$ipaddress = $_SERVER["REMOTE_ADDR"];
$date = date("Y-m-d H:i:s");

$nyelv_tomb = array("hu", "de", "ro");

require('adbconnect.php');
include("optika.php"); 


if(!isset($_SESSION['nyelv'])){
	$_SESSION['nyelv']="hu";
}

//2020.11.27 lang/nyelv választás override:
$_SESSION['nyelv']="hu";


$nyelv_key = array_search($_SESSION['nyelv'], $nyelv_tomb);
//echo $nyelv_key; 
unset($nyelv_tomb[$nyelv_key]);
$nyelv_tomb = array_values($nyelv_tomb);
//print_r($nyelv_tomb);



require_once 'mobildetect/Mobile_Detect.php';

$DataForm_cim=array('../up_image/',''); 
$FormData_cim=array('/up_image/','');


$detect = new Mobile_Detect;
if ( $detect->isMobile() ) {
	$_SESSION['mobil']=1;	
} else {
	$_SESSION['mobil']=0;
}

if(isset($_GET['se_page'])){
	$page=$_GET['se_page'];
	}
	else {
	$page=$_POST['se_page'];
}

if (empty($_SESSION['cikkek'])){
$my_array=array();
$_SESSION['cikkek']=$my_array;
}

if (empty($_SESSION['darab'])){
$my_array=array();
$_SESSION['darab']=$my_array;
}
function randomString($length = 10, $chars = '1234567890') {  
  
    if ($chars == 'alphalower') {  
       $chars = 'abcdefghijklmnopqrstuvwxyz';  
    }   
    if ($chars == 'numeric') {  
         $chars = '1234567890';  
   }  
  
    if ($chars == 'alphanumeric') {  
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';  
    }  
    if ($chars == 'hex') {  
       $chars = 'ABCDEF1234567890';  
    }  
    $charLength = strlen($chars)-1;  
  
     for($i = 0 ; $i < $length ; $i++)  
        {  
            $randomString .= $chars[mt_rand(0,$charLength)];  
        }  
   
     return $randomString;  
}
if(isset($_POST['sr'])){
	$sorrend = $_POST['sr'];
	if($sorrend == 1){
		$_SESSION['sorrendje']="termekek.Akt_ara DESC";			
	}
	else if($sorrend == 2){
		$_SESSION['sorrendje']="termekek.Akt_ara ASC";			
	}
	else if($sorrend == 3){
		$_SESSION['sorrendje']="termekek.Id DESC";			
	}
	else if($sorrend == 4){
		$_SESSION['sorrendje']="termekek.Id ASC";			
	}
}
if(!isset($_SESSION['sorrendje'])){
	$_SESSION['sorrendje']= "termekek.Akt_ara ASC";
}

function get_current_url($strip = true) {
    // filter function
    static $filter;
    if ($filter == null) {
        $filter = function($input) use($strip) {
            $input = str_ireplace(array(
                "\0", '%00', "\x0a", '%0a', "\x1a", '%1a'), '', urldecode($input));
            if ($strip) {
                $input = strip_tags($input);
            }
            // or any encoding you use instead of utf-8
            $input = htmlspecialchars($input, ENT_QUOTES, 'utf-8'); 
            return trim($input);
        };
    }

    //return 'http'. (($_SERVER['SERVER_PORT'] == '443') ? 's' : '').'://'. $_SERVER['SERVER_NAME'] . $filter($_SERVER['REQUEST_URI']);
	return $filter($_SERVER['REQUEST_URI']);
}
$alaplink_all = get_current_url();
$rest_link = substr($alaplink_all, -1); 
	if($rest_link == "/"){
		$alaplink_all = substr($alaplink_all, 0, -1);	
	} else {
	$alaplink_all = get_current_url();
	}
if($mod == "out"){ 

//echo $hibauzi_login;
 unset($_SESSION['sess_site_id_for_site']); 
 unset($_SESSION['sess_site_nev']);
 unset($_SESSION['sess_user_szint']);
 unset($_SESSION['cikkek']);
 unset($_SESSION['darab']);
 unset($_SESSION['meret']);
 unset($_SESSION['torzs_levon']);
 unset($_SESSION['sess_veznev_user']);
 unset($_SESSION['sess_kernev_user']);
 unset($_SESSION['termek_ar']);
 unset($_SESSION['termek_cikkszam']); 
}


if(isset($_POST['melyik_ter'])){
	$melyik = $_POST['melyik_ter']; 
	$drb_sz = $_POST['drb_sz'];	
	$_SESSION['darab'][$melyik]= $drb_sz;
}

$del_nem = $_POST['del_ter'];
if(isset($del_nem)){
	array_splice($_SESSION['cikkek'], $del_nem,1);
	array_splice($_SESSION['darab'], $del_nem,1);
	array_splice($_SESSION['meret'], $del_nem,1);
}
if(isset($_GET['se_szo'])){
	$se_szo=$_GET['se_szo'];
} else {
	$se_szo=$_POST['se_szo'];
}

$z=0;
$lekerd_kezdo_swiss_gyarto_idk = "";
$lekerd_kezdo_swiss_gyarto_nevek = "";
	$query="SELECT Id,Nev FROM gyarto ORDER BY Id DESC";
	$result=mysql_query($query);
	$num=mysql_numrows($result);
	$i=0;
	while ($i < $num) {
	$Id=mysql_result($result,$i,"Id");
	$gyarto_nev=mysql_result($result,$i,"Nev");
	$aktiv_hu = 0;

		$result_dwn = mysql_query("SELECT Id,Aktiv FROM lang_gyartok WHERE Gyarto_id=$Id AND Nyelv = '$_SESSION[nyelv]'");
		$row = mysql_fetch_assoc($result_dwn);
		$belep_id = $row['Id'];
		$aktiv_hu = $row['Aktiv'];
		if($aktiv_hu == 1){
			if($z == 0){
			$lekerd_kezdo_swiss_gyarto_idk .= " AND (gyarto.Id =$Id";
			} else {
			$lekerd_kezdo_swiss_gyarto_idk .= " OR gyarto.Id =$Id";
			}		
			$lekerd_kezdo_swiss_gyarto_nevek .= $gyarto_nev." - ";
		$z++;
		}


		if($i == $num-1){
			$lekerd_kezdo_swiss_gyarto_idk .= ")";
		}
		
	$i++;
}

//echo $_SESSION['sess_user_szint'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $all_site_title ?></title>
<meta name="description" content="<?= $all_site_descr ?>">
<meta name="keywords" content="<?= $all_site_keywords ?>">
<!--<link rel="preload" as="font" type="font/woff2" crossorigin href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/fonts/fontawesome-webfont.woff2?v=4.3.0"/> -->
<link rel="preload" as="font" type="font/woff2" crossorigin href="/fonts/fontawesome-webfont.woff2?v=4.3.0"/>
<link rel="preload" as="font" type="font/woff" crossorigin href="/fonts/fontawesome-webfont.woff"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="Zhv38E4zoHcavwOc2ry2lB36cVoZvfKwb1oel1dF25c" /> <!-- google merchant identification html meta tag-->
<!-- GTM 2023 -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P7F9G5BS');</script>
<!-- End Google Tag Manager -->
<!-- <script defer src="/jquery-latest.js"></script> -->
<script>
//var site_width = $(window).width();
//var site_height = $(window).height();
var site_width = window.innerWidth;
var site_height = window.innerHeight;
  var myWidth = 0, myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }

function scrollToAnchor(aid){
	//alert(aid);
	var aTag = $("a[name='"+ aid +"']");
	$('html,body').animate({scrollTop: aTag.offset().top - 0},500);
}
function checkPassword(str){
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

  var ablak_width = 0, ablak_height = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    ablak_width = window.innerWidth;
    ablak_height = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    ablak_width = document.documentElement.clientWidth;
    ablak_height = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    ablak_width = document.body.clientWidth;
    ablak_height = document.body.clientHeight;
  }


</script>
<link href='https://fonts.googleapis.com/css?family=Playfair Display:200,300,400,500,600,700,800&amp;subset=latin-ext' rel='stylesheet'>
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Poppins:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&amp;subset=latin-ext" rel="stylesheet"> 
<script defer src="/script_3.js"></script>
<script defer type="text/javascript" src="/jquery_10.1.js"></script>
<script defer type="text/javascript" src="/jquery_10.1_ui.js"></script>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> <!--ez nem lehet defer, mert akkor nem megy a logo slide-->
<script defer src="/elevatezoom/jquery.elevatezoom.js" type="text/javascript"></script>

<link defer href="/allstyle_v2.css" rel="stylesheet" type="text/css"/>
<!-- 
<link rel="stylesheet" href="/fancybox/source/jquery.fancybox.css?v=2.1.6" type="text/css" media="screen" />
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Cabin:400,500,600,700&amp;subset=latin-ext" rel="stylesheet"> 
<script defer type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js?v=2.1.6"></script> 
-->

<!-- Meta Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1116639932743300');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1116639932743300&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
<style>

@media only screen and (max-width: 3000px) {
#langbox_cont{width:40px; height:40px;position: absolute; z-index:154664564;top: 50%; -webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);text-align:center; right:0px; background-color:#963e3e; display:table;}
.langbox_cont_ins{width:100%; text-align:center; display:table-cell; vertical-align:middle;font-family: 'Poppins', sans-serif;font-size:16px; color:#FFF; text-transform:uppercase;}
.langbox_cont_ins a{color:#FFF;background-color:transparent;padding:0px;text-decoration:none;-webkit-transition: 0.3s ease-in-out all;-moz-transition: 0.3s ease-in-out all;-o-transition: 0.3s ease-in-out all;transition: 0.3s ease-in-out all; padding:0px 0px 0px 0px;}
.langbox_cont_ins a:hover{color:#FFF;padding:0px;text-decoration:none; padding:0px 0px 0px 0px;}
.langbox_cont_for_other_langs{width:40px; height:40px;position: relative; z-index:1435634;text-align:center; left:0px; background-color:#963e3e; display:table;}
#langbox_cont_oth_conts{width:40px; position:absolute; z-index:234534534; margin:40px 0px 0px 0px; display:none;}

}

@media only screen and (max-width: 1024px) { 

}


@media only screen and (max-width: 768px){
#langbox_cont{width:40px; height:40px;position: absolute; z-index:154664564;top: 20%; -webkit-transform: translateY(-20%);-ms-transform: translateY(-20%);transform: translateY(-20%);text-align:center; left:0px; background-color:#963e3e; display:table;}
}

@media only screen and (max-width: 479px){

}
</style>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P7F9G5BS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<? include("site_logon.php"); ?>
<div class="sws_middle_header_cont">
	<!-- +++logo header block start+++ -->
	<div class="sws_middle_header_cont_logo_cont">
    	<a href="/"><img src="/images/logo.png" style="" /></a>
    </div>
    <!-- +++logo header block end+++ -->

	<!-- +++menu block start+++ -->
<div class="sws_allmenu_main_cont">
	<div class="tsw_menu_ins">
        <ul>
          <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ferfi/ora/">Férfi óra</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/om/ceas/">Om Ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/herren/uhr/">Herren Uhr</a><? } ?></h1></li>
          <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/noi/ora/">Női óra</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/femeie/ceas/">Femeie Ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/damen/uhr/">Damen Uhr</a><? } ?></h1></li>
          <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/tovabbi-orak">Egyéb órák</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/altii-ceas">Alții ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/andere-uhren">Andere Uhren</a></h1><? } ?></li>
          <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/oraszij">Óraszíj</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/curea-de-ceas">Curea de ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/uhrenarmband">Uhrenarmband</a><? } ?></h1></li>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ongyujto">Öngyújtó</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/bricheta">Brichetă</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/Feuerzeug">Feuerzeug</a><? } ?></h1></li>			
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/szemuveg">Szemüveg</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/ochelari">Ochelari</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/brille">Brille</a><? } ?></h1></li>
          <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/penztarca">Pénztárca</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/portofel">Portofel</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/brieftasche">Brieftasche</a><? } ?></h1></li>
		</ul>     
    </div>
	<div class="tsw_menu_ins">
		<ul>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/taskak">Táska</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/taskak">Táskák</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/taskak">Táskák</a><? } ?></h1></li>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/parfum">Parfüm</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/parfum">parfum</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/parfum">Parfum</a><? } ?></h1></li>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ekszer">Ékszer</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/bijuterie">Bijuterie</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/juwel">Juwel</a><? } ?></h1></li>
  		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ekszer/dragakoves-ekszer">Drágaköves ékszer</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/ekszer/dragakoves-ekszer">Drágaköves ékszer</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/ekszer/dragakoves-ekszer">Drágaköves ékszer</a><? } ?></h1></li>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ekszer/karikagyuru">Karikagyűrű</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/ekszer/karikagyuru">KGY</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/ekszer/karikagyuru">KGY</a><? } ?></h1></li>
		  <li><h1><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ekszer/eljegyzesigyuru">Eljegyzesi gyűrű</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/ekszer/eljegyzesigyuru">EGY</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/ekszer/eljegyzesigyuru">EGY</a><? } ?></h1></li>
		</ul>     
    </div>
</div>
<!-- +++menu block end+++ -->

	<!-- +++keresés block start+++ -->
	<div class="sws_middle_header_cont_search_cont">
                                    <form method="POST" action="/kereses" name="keerso_div" id="keerso_div" enctype="multipart/form-data" style="margin:0px; padding:0px;">
                                        <div class="se_menu_se_back">
                                            <div class="new_se_box_left">
<? if($_SESSION['nyelv'] == "hu"){ ?>
<input type="text" name="se_szo" id="se_szo" <? if($se_szo != ""){ ?>value="<?= $se_szo ?>"<? } else { ?>value="termék kereső"<? } ?> class="new_se_box_input" onclick="javascript: if(document.forms['keerso_div'].elements['se_szo'].value=='termék kereső') document.forms['keerso_div'].elements['se_szo'].value='';" onblur="javascript: if(document.forms['keerso_div'].elements['se_szo'].value=='') document.forms['keerso_div'].elements['se_szo'].value='termék kereső';" autocomplete="off" onkeyup="sitesorch()" />
<? } ?>
                                                                                            </div>
                                            <div class="new_se_box_right" onclick="nev_keres()">
                                                <div class="new_se_box_right_ins">
                                                    <div class="new_se_box_right_ins_lupe_cont">
                                                        <div class="new_se_box_right_ins_lupe_cont_ins"><img src="/images/icon_se.png" /></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
<script language="javascript" type="text/javascript">
	function nev_keres(){
		if(document.getElementById("se_szo").value != ""){
			document.forms["keerso_div"].submit();
		}
	}

	function conf_site_serch(num){
		$( "#se_szo" ).val(num);
		document.forms["keerso_div"].submit();
	}

	function sitesorch(){
		se_szoval =$("#se_szo").val();
		se_szoval_hossz = se_szoval.length;

			var request = $.ajax({
				url: "/ax_searchbox_help.php",
				data: {'se_szo': se_szoval,'hoszz': se_szoval_hossz},
				type: "GET",           
				dataType: "html"
			});
	 
			request.done(function(msg) {
				//alert(msg);
				myArr = msg.split("%/");
				
				u_id = eval(myArr[1]);
				back_html = myArr[2];
				//alert(back_html);
				if(u_id == 1){
					$("#se_cont_felso").html(back_html);
					if($("#se_show_se_stroy_allcont").is(":hidden")){
						$("#se_show_se_stroy_allcont").slideToggle(200, function () {
							
						});
					}
				} else {
				
				}
			});
	 
			request.fail(function(jqXHR, textStatus) {
				//alert( "Request failed: " + textStatus );
			});


	}
$("#se_show_se_stroy_allcont").mouseleave(function() {
		if($("#se_show_se_stroy_allcont").is(":visible")){
			$("#se_show_se_stroy_allcont").slideToggle(100, function () {
				
			});
		}
});
</script>  
    </div>
    
    
<?
$array_count = count($_SESSION['cikkek']);
$sum_db = 0;
for ($i=0;$i<$array_count;$i++){ 
$darab_sz = $_SESSION['darab'][$i];
$sum_db = $sum_db + $darab_sz;
}
?>
<!-- +++keresés block end+++ -->

<!-- +++kosár block start+++ -->
	<div class="topm_topmenu_middle_allcont_rgt_rgt" >
		<div class="topm_topmenu_middle_allcont_rgt_rgt_for_cart_cont">
			<div id="tpm_lilt_kos_container">
			<? if($array_count > 0){ ?>
<?    
	$sum_ar = 0;
    $szallitas_szallitok = array();
	$szallitas_erteke = array();
    for ($i=0;$i<$array_count;$i++){ 
    $ter_id = $_SESSION['cikkek'][$i];
    $darab_sz = $_SESSION['darab'][$i];
    $colorja = $_SESSION['meret'][$i];
    
	$result_dwn = mysql_query("SELECT termekek.Termek_nev,termekek.Cikkszam,termekek.Gyarto,termekek.Fokat_id,termekek.Alkat_id,termekek.Alkat_id,termekek.Subkat_id,termekek.Akt_ara,termekek.Akcios,termekek.Orig_ara,termekek.nev_opt,termekek.Forgalmazo,termekek.Viszont_ara FROM termekek WHERE Id=$ter_id");
	$row = mysql_fetch_assoc($result_dwn);
	$con_neve_mod = $row['Termek_nev'];
	$con_cikkszam_mod = $row['Cikkszam'];
	$con_gyarto_mod = $row['Gyarto'];
	$con_fokat_mod = $row['Fokat_id'];
	$con_alkat_mod = $row['Alkat_id'];


	$con_mod_ara = $row['Akt_ara'];
	$con_mod_kcio = $row['Akcios'];
	$con_mod_origara = $row['Orig_ara'];
	$con_mod_forgalmazo = $row['Forgalmazo'];	

	$con_con_nev_opt = $row['nev_opt'];
	if($_SESSION['sess_user_szint'] == 1){								
		$con_mod_ara = $row['Viszont_ara'];
		$con_mod_kcio=0;								
	} 
	if($_SESSION['nyelv'] != "hu"){ 
		$result_dwn = mysql_query("SELECT Nev,nev_opt,Leiras FROM lang_termkdets WHERE Termek_id=$ter_id AND Nyelv = '$_SESSION[nyelv]'");
		$row = mysql_fetch_assoc($result_dwn);
		$con_neve_mod = $row['Nev'];	
		if($_SESSION['nyelv'] == "ro"){ 
			$result_dwn = mysql_query("SELECT Ro_penznem,Ro_valt FROM alapok WHERE Id=1");
			$row = mysql_fetch_assoc($result_dwn);
			$Penznem = $row['Ro_penznem']."&nbsp;";
			$Ro_valt = $row['Ro_valt'];		
			$valto_akt_ara = $con_mod_ara * $Ro_valt; 
			$con_mod_ara = round($valto_akt_ara + 0.01) - 0.01;
			if($con_mod_kcio == 1){
				$valto_akcios_ara = $con_mod_origara * $Ro_valt; 
				$con_mod_origara = round($valto_akcios_ara + 0.01) - 0.01;					
			}
				
		} else if($_SESSION['nyelv'] == "de"){ 
			$result_dwn = mysql_query("SELECT De_penznem,De_valt FROM alapok WHERE Id=1");
			$row = mysql_fetch_assoc($result_dwn);
			$Penznem = $row['De_penznem']."&nbsp;";
			$De_valt = $row['De_valt'];		
			$valto_akt_ara = $con_mod_ara * $De_valt; 
			$con_mod_ara = round($valto_akt_ara + 0.01) - 0.01;
			if($con_mod_kcio == 1){
				$valto_akcios_ara = $con_mod_origara * $De_valt; 
				$con_mod_origara = round($valto_akcios_ara + 0.01) - 0.01;					
			}			
			
		}	
	}
	$akt_ar = $con_mod_ara * $darab_sz;
	$sum_ar = $sum_ar + $akt_ar;
if (in_array($con_mod_forgalmazo, $szallitas_szallitok)) {
	$key = array_search($con_mod_forgalmazo,$szallitas_szallitok);
	$szallitas_erteke[$key] = $szallitas_erteke[$key]+$akt_ar;
} else {

array_push($szallitas_szallitok, $con_mod_forgalmazo);
array_push($szallitas_erteke, $akt_ar);

}
?>
<div class="tpm_lilt_kos_item_blocks">
	<div class="tpm_lilt_kos_dbszam"><?= $darab_sz ?>x</div>
    <div class="tpm_lilt_kos_termek_nev"><?= $con_neve_mod ?></div>
    <div class="tpm_lilt_kos_termek_ar"><span><? if($_SESSION['nyelv'] == "hu"){ ?><?= number_format(floatval($akt_ar),0,",",".") ?>.-<? } else { ?><?= $akt_ar ?><? } ?>.-</span></div>
</div>
<? } ?>
<? 
$kos_szall = 0;
//echo "SELECT Ar FROM arsavok WHERE $sum_ar BETWEEN Min_db AND Max_db";
if($_SESSION['nyelv'] == "hu"){
	$result_dwn = mysql_query("SELECT Ar FROM arsavok WHERE $sum_ar BETWEEN Min_db AND Max_db");
	$row = mysql_fetch_assoc($result_dwn);	
	$szallitasi_dij = $row['Ar'];
} else if($_SESSION['nyelv'] == "ro"){ 
	$result_dwn = mysql_query("SELECT Ar FROM arsavok_ro WHERE $sum_ar BETWEEN Min_db AND Max_db");
	$row = mysql_fetch_assoc($result_dwn);	
	$szallitasi_dij = $row['Ar'];
	//echo "sfd";
				
} else if($_SESSION['nyelv'] == "de"){ 
	$result_dwn = mysql_query("SELECT Ar FROM arsavok_de WHERE $sum_ar BETWEEN Min_db AND Max_db");
	$row = mysql_fetch_assoc($result_dwn);	
	$szallitasi_dij = $row['Ar'];		
			
} 
	//$szallitasi_dij = 990;
	$kos_szall = $kos_szall + $szallitasi_dij;
?>
										<div class="tpm_lilt_kos_szallitasi_dij"><? if($_SESSION['nyelv'] == "hu"){ ?>Szállítási díj<? } else if($_SESSION['nyelv'] == "ro"){ ?>Taxa de expediere<? } if($_SESSION['nyelv'] == "de"){ ?>Versandkosten<? } ?>: <span ><?= $kos_szall ?></span>.-</div>
<? $sum_ar = $sum_ar + $kos_szall; ?>                                        
                                        <div class="tpm_lilt_kos_vege_cont">
                                        	<div class="tpm_lilt_kos_vegosszeg_lft"><? if($_SESSION['nyelv'] == "hu"){ ?>Végösszeg<? } else if($_SESSION['nyelv'] == "ro"){ ?>Total<? } if($_SESSION['nyelv'] == "de"){ ?>Gesamt<? } ?>:</div>
											<div class="tpm_lilt_kos_vegosszeg_rgt"><span ><? if($_SESSION['nyelv'] == "hu"){ ?><?= number_format(floatval($sum_ar),0,",",".") ?><? } else { ?><?= $sum_ar ?><? } ?></span>.-</div>
                                        </div>
                                        <a href="/kosar">
                                    	<div class="tpm_lilt_kos_vegosszeg_fizetek">
                                    		<div class="tpm_lilt_kos_vegosszeg_fizetek_text"><? if($_SESSION['nyelv'] == "hu"){ ?>megrendel<? } else if($_SESSION['nyelv'] == "ro"){ ?>bestellen<? } if($_SESSION['nyelv'] == "de"){ ?>bestellen<? } ?> <span>&#xf101;</span></div>
                                        </div>
                                        </a>
                                        <? } else { ?>
                                        <div class="tpm_lilt_kos_empty_text"><? if($_SESSION['nyelv'] == "hu"){ ?>Üres a kosara<? } else if($_SESSION['nyelv'] == "ro"){ ?>Coșul dvs. este momentan gol<? } if($_SESSION['nyelv'] == "de"){ ?>Ihr Warenkorb ist derzeit leer<? } ?>.</div>
                                        <? } ?>	
                                    </div>
                                </div>
                            	<div class="topm_topmenu_middle_allcont_rgt_rgt_iconcont">
                                <? if($array_count > 0){ ?>                                
                                <a href="/kosar"><div style="font-size: 24px; font-family: 'awesome', sans-serif; color: #FFFFFF;">&#xf07a;</div></a>
                                <? } else { ?>
									<div style="font-size: 24px; font-family: 'awesome', sans-serif; color: #FFFFFF;">&#xf07a;</div>
                                <? } ?>
                                </div>
                          	</div>
                            


<script>
$(".topm_topmenu_middle_allcont_rgt_rgt").mouseenter(function() {
	$( ".topm_topmenu_middle_allcont_rgt_rgt_for_cart_cont" ).fadeIn( "slow", function() {
	});
  })
  .mouseleave(function() {
	$( ".topm_topmenu_middle_allcont_rgt_rgt_for_cart_cont" ).fadeOut( "fast", function() {
	});
});
<? if($array_count > 0){ ?>
$(".topm_topmenu_middle_allcont_rgt_rgt_iconcont").click(function() {
	document.location.href = "/kosar";
});
<? } ?>

</script>    
</div>
</div>
<!-- +++kosár block end+++ -->




<!-- +++mobile menu block start+++ -->
<div class="sws_allmenu_main_cont_formobil"><? include("topmenu_new.php"); ?></div>
<!-- +++mobile menu block end+++ -->

<!-- +++module inclues block start+++ -->
<?
//echo $mod;
		if($mod=="se"){
		include("search.php"); 
		}
		else if($mod=="tr"){
		include("tartalmak.php"); 
		} 
		else if($mod=="terd"){
		include("termek_det.php"); 
		} 
		else if($mod=="ks"){
		include("kosar.php"); 
		} 
		else if($mod=="reg"){
		include("regisztracio.php"); 
		}
		else if($mod=="ks_next"){
		include("kosar_summa.php"); 
		}  
		else if($mod=="ks_end"){
		include("kosar_save.php"); 
		} 
		else if($mod=="ks_er"){
		include("kosar_erosit.php"); 
		} 
/*
		else if($mod=="kmp"){
		include("kampany.php"); 
		} 
*/
		else if($mod=="kmp"){
		include("kampany.php"); 
		} 
		else if($mod=="akciok"){
		include("akciok.php"); 
		} 
		else if($mod=="akciok_se"){
			include("akciok_search.php"); 
		} 
		else if($mod=="tszt"){
		include("teszt.php"); 
		} 
		else if($mod=="creg"){
		include("regisztracioceg.php"); 
		} 
		else if($mod=="frg"){
		include("forgot_pass.php"); 
		} 
		else if($mod=="prf"){
		include("profil.php"); 
		} 
		else if($mod=="zip"){
		include("kepcreate.php"); 
		} 
		else {
	 	include("kezdo.php");
		} 
?>
<? include("all_bottom.php"); ?>
<!-- +++module inclues block end+++ -->
</body>
</html>