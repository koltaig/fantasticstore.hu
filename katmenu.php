<?
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");

?>
<? if($_SESSION['mobil'] == 0){ ?>
<style>


@media only screen and (max-width: 3000px) {
.fokat_menu_none{font-family: 'Poppins', sans-serif;font-size:17px; color:#FFFFFF; padding:5px 9px 4px 0px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase;}
.fokat_menu_none a {color: #FFFFFF;text-decoration: none;}
.fokat_menu_none a:hover {color: #FFFFFF;text-decoration: none;}
.fokat_menu{font-family: 'Poppins', sans-serif;font-size:17px; color:#FFFFFF; padding:5px 9px 4px 10px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase; z-index:3000;}
.fokat_menu a {color: #FFFFFF;text-decoration: none;}
.fokat_menu a:hover {color: #5d0716;text-decoration: none;}

.fokat_menu_y{font-family: 'Poppins', sans-serif;font-size:17px; color:#FFFFFF; padding:5px 9px 4px 10px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase; z-index:3000;}
.fokat_menu_y a {color: #ff9494;text-decoration: none;}
.fokat_menu_y a:hover {color: #5d0716;text-decoration: none;}


.menu_simple ul {
    margin: 0; 
    padding: 0;
    width:200px;
    list-style-type: none;
	text-align:left;
	font-family: 'Poppins', sans-serif;font-size:15px; color:#2a2a2a;
}

.menu_simple ul li a {
    text-decoration: none;
    color: #2a2a2a; 
    padding:4px 10px 4px 5px;
	margin:3px 0px 3px 0px;
    background-color: #FFFFFF;
    display:block;
}
 
.menu_simple ul li a:visited {
    color: #2a2a2a;
}
 
.menu_simple ul li a:hover, .menu_simple ul li .current {
    color: #2a2a2a;
    background-color: #c3c3c3;
	-webkit-transition: 0.2s ease-in-out all;
    -moz-transition: 0.2s ease-in-out all;
    -o-transition: 0.2s ease-in-out all;
    transition: 0.2s ease-in-out all;
}
.fokat_menu_sub_menu{position:absolute; width:auto; z-index:200;  background-color:#FFFFFF; clear:both; display:none; overflow:hidden; margin-left:-10px;}
.fokat_sub_kats_alldiv{width:200px; margin-left:10px; float:left;}
.fokat_kathead{margin-top:10px; margin-bottom:10px; color:#a20000; text-transform:uppercase; border-bottom:1px solid #a20000; padding-bottom:4px;font-family: 'Poppins', sans-serif;font-size:17px; text-align:left; font-weight:bold;}
.fokat_kathead a{
	color:#a20000;
	text-decoration:none;
}
.fokat_kathead  a:hover{
	color:#d40000;
	text-decoration:none;
}
.fokat_menu_img{font-family: 'Poppins', sans-serif;font-size:21px; color:#FFFFFF; padding:2px 3px 0px 3px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase;}
.menu_all_more_link{width:100%; clear:both; border-top:1px solid #820600; text-align:center;font-family:'Poppins', Verdana, Helvetica;font-size:14px; color:#820600; padding:10px 0px 10px 0px; font-weight:normal; cursor:pointer;}
}

@media only screen and (max-width: 1024px) { 
.fokat_menu_none{font-family: 'Poppins', sans-serif;font-size:14px; color:#FFFFFF; padding:6px 10px 6px 0px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase;}
.fokat_menu_none a {color: #FFFFFF;text-decoration: none;}
.fokat_menu_none a:hover {color: #FFFFFF;text-decoration: none;}
.fokat_menu{font-family: 'Poppins', sans-serif;font-size:14px; color:#FFFFFF; padding:6px 10px 6px 10px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase; z-index:3000;}
.fokat_menu a {color: #FFFFFF;text-decoration: none;}
.fokat_menu a:hover {color: #5d0716;text-decoration: none;}

.fokat_menu_y{font-family: 'Poppins', sans-serif;font-size:14px; color:#FFFFFF; padding:6px 10px 6px 10px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase; z-index:3000;}
.fokat_menu_y a {color: #ff9494;text-decoration: none;}
.fokat_menu_y a:hover {color: #5d0716;text-decoration: none;}


.menu_simple ul {
    margin: 0; 
    padding: 0;
    width:200px;
    list-style-type: none;
	text-align:left;
	font-family: 'Poppins', sans-serif;font-size:15px; color:#2a2a2a;
}

.menu_simple ul li a {
    text-decoration: none;
    color: #2a2a2a; 
    padding:4px 10px 4px 5px;
	margin:3px 0px 3px 0px;
    background-color: #FFFFFF;
    display:block;
}
 
.menu_simple ul li a:visited {
    color: #2a2a2a;
}
 
.menu_simple ul li a:hover, .menu_simple ul li .current {
    color: #2a2a2a;
    background-color: #c3c3c3;
	-webkit-transition: 0.2s ease-in-out all;
    -moz-transition: 0.2s ease-in-out all;
    -o-transition: 0.2s ease-in-out all;
    transition: 0.2s ease-in-out all;
}
.fokat_menu_sub_menu{position:absolute; width:auto; z-index:200;  background-color:#FFFFFF; clear:both; display:none; overflow:hidden; margin-left:-10px; margin-top:-2px;}
.fokat_sub_kats_alldiv{width:200px; margin-left:10px; float:left;}
.fokat_kathead{margin-top:10px; margin-bottom:10px; color:#6c0b1c; text-transform:uppercase; border-bottom:1px solid #6c0b1c; padding-bottom:4px;font-family: 'Poppins', sans-serif;font-size:17px; text-align:left; font-weight:bold;}
.fokat_menu_img{font-family: 'Poppins', sans-serif;font-size:18px; color:#FFFFFF; padding:2px 3px 0px 3px;position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; float:left; text-transform:uppercase;}
}


@media only screen and (max-width: 768px){

}

@media only screen and (max-width: 479px){
/* lila címke */

}

</style>
<div style="width:100%; height:46px; text-align:center; background-color:#2a2a2a; border-bottom:2px solid #FFFFFF; clear:both; position:relative;">
	<div style=" width:100%; max-width:1400px; margin:0px auto;height:46px;position:relative;z-index:2000; ">
    	<div class="fokat_menu_none"><a href="/">kezdőlap</a></div>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
    	<div class="fokat_menu" >
        	<div class="fokat_menu_sub_menu" id="fokat_ff">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >cipő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=1 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ferfi/cipo/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >kiegészítő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =2 AND (termekek.Neme=1 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ferfi/kiegeszito/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
             	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >márkák</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=1 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY gyarto.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$gyarto_id=mysql_result($result,$i,"termekek.Gyarto");
$gyarto_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ferfi/<?= $gyarto_nev_opt ?>/" ><?= $gyarto_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                <div style="width:100%; clear:both; border-top:1px solid #003333;">
                	sdfsdf
                </div>                 
            </div>
        <a href="/ferfi/">férfi</a></div>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu" >
        	<div class="fokat_menu_sub_menu" id="fokat_noi">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >cipő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=2 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/noi/cipo/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >kiegészítő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =2 AND (termekek.Neme=2 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/noi/kiegeszito/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
             	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >márkák</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=2 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY gyarto.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$gyarto_id=mysql_result($result,$i,"termekek.Gyarto");
$gyarto_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/noi/<?= $gyarto_nev_opt ?>/" ><?= $gyarto_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                <div style="width:100%; clear:both; border-top:1px solid #003333;">
                	sdfsdf
                </div>                 
                
            </div>        
        
        
        <a href="/noi/">női</a></div>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu" >
        	<div class="fokat_menu_sub_menu" id="fokat_gyerek">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >cipő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND termekek.Neme=3 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/gyermek/cipo/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >márkák</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND termekek.Neme=3 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY gyarto.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$gyarto_id=mysql_result($result,$i,"termekek.Gyarto");
$gyarto_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/gyermek/<?= $gyarto_nev_opt ?>/" ><?= $gyarto_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
                
            </div>        
        
        
        <a href="/gyermek/">gyermek</a></div>
        
<!-- textil kezd -->     

        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu" >
        	<div class="fokat_menu_sub_menu" id="fokat_textil">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >kategóriák</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =5 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ruhazat/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >márkák</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =5 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY gyarto.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$gyarto_id=mysql_result($result,$i,"termekek.Gyarto");
$gyarto_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ruhazat/<?= $gyarto_nev_opt ?>/" ><?= $gyarto_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
                
            </div>        
        
        
        <a href="/ruhazat/">ruházat</a></div>        
<!-- textil véeg -->        
        
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu" >
        	<div class="fokat_menu_sub_menu" id="fokat_marka">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >cipő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=1 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/cipo/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>  
                    
                    <div class="menu_simple" id="marka_cipo" style=" display:none;overflow: hidden;">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=1 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10,1000";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/cipo/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                      
                                  	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >kiegészítő</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE  meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=2 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/kiegeszito/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div> 
                    <div class="menu_simple" id="marka_kieg" style="display:none;overflow: hidden;">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE  meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=2 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10,1000";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/kiegeszito/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>  
                
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" >ruházat</div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE  meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=5 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ruhazat/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div> 
                    <div class="menu_simple" id="ruha_kieg" style="display:none;overflow: hidden;">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt, COUNT(termekek.Id) as count FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE  meret_ter.Darabszam > 0 AND termekek.Active=1 AND termekek.Termek_tipus=5 $lekerd_kezdo_ebard_gyarto_idk GROUP BY gyarto.Nev ORDER BY count DESC LIMIT 10,1000";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ruhazat/<?= $gyarto_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
                              
                <div style="" class="menu_all_more_link" onclick="marka_more_open()">
                	még több márka
                </div>                  
            </div>        
        
        
        <a href="#">márkák</a></div>
	<script>
function marka_more_open(){
    $('#marka_kieg').slideToggle(200, function () {
        // 
    });
    $('#marka_cipo').slideToggle(200, function () {
        // 
    });
    $('#ruha_kieg').slideToggle(200, function () {
        // 
    });
}
		</script>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu">
        	<div class="fokat_menu_sub_menu" id="fokat_orias">
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" ><a href="/orias-termekek/ferfi/" >férfi</a></div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=1 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 AND (meret_ter.Meret_id=33 OR meret_ter.Meret_id=59 OR meret_ter.Meret_id=143 OR meret_ter.Meret_id=50 OR meret_ter.Meret_id=53 OR meret_ter.Meret_id=18 OR meret_ter.Meret_id=61 OR meret_ter.Meret_id=95 OR meret_ter.Meret_id=64 OR meret_ter.Meret_id=99) $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/orias-termekek/ferfi/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>
                
            	<div class="fokat_sub_kats_alldiv">
                	<div class="fokat_kathead" ><a href="/orias-termekek/noi/" >női</a></div>
                    <div class="menu_simple">
                    <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND (termekek.Neme=2 OR termekek.Neme=4) AND termekek.Active=1 AND meret_ter.Darabszam > 0 AND (meret_ter.Meret_id=12 OR meret_ter.Meret_id=32 OR meret_ter.Meret_id=24 OR meret_ter.Meret_id=15 OR meret_ter.Meret_id=63 OR meret_ter.Meret_id=6 OR meret_ter.Meret_id=3 OR meret_ter.Meret_id=43 OR meret_ter.Meret_id=39 OR meret_ter.Meret_id=5 OR meret_ter.Meret_id=4 OR meret_ter.Meret_id=27 OR meret_ter.Meret_id=38 OR meret_ter.Meret_id=9 OR meret_ter.Meret_id=26 OR meret_ter.Meret_id=33 OR meret_ter.Meret_id=59 OR meret_ter.Meret_id=50 OR meret_ter.Meret_id=53) $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/orias-termekek/noi/<?= $kat_nev_opt ?>/" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                    </ul>
                    </div>                	
                </div>                
                
            </div> 
        	<a href="/orias-termekek">Óriás termékek</a>
        </div>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu"><a href="/brand-club">Ebrand club</a></div>
        <div class="fokat_menu_img"><img src="/images/menu_v.jpg" /></div>
        <div class="fokat_menu_y" ><a href="/termekcsere-garancia">Termékcsere, garancia</a></div>
        
    </div>
</div>


      <script type = "text/javascript" language = "javascript">
         $(document).ready(function() {

            $(".fokat_menu").hover(
				
               function () {
			   	  $(this).css({"background-color":"white"});
				  $(this).children("a").css({"color":"#5d0716"});
				  $(this).children(".fokat_menu_sub_menu").css('margin-top',"23px");
				  $(this).children(".fokat_menu_sub_menu").toggle(100,  function () {
        // 
    			  });
				
				  
               }, 
				
               function () {
                  $(this).css('background-color','transparent');
				  $(this).children("a").css({"color":"#FFFFFF"});	
				  $(this).children(".fokat_menu_sub_menu").css("display","none");
               }
            );
			
            $(".fokat_menu_y").hover(
				
               function () {
			   	  $(this).css({"background-color":"white"});
				
				  
               }, 
				
               function () {
                  $(this).css('background-color','transparent');

               }
            );			
			
			$("#fokat_ff").width( 640 );
			$("#fokat_noi").width( 640 );
			$("#fokat_gyerek").width( 430 );
			$("#fokat_marka").width( 640 );
			$("#fokat_orias").width( 430 );
			$("#fokat_textil").width( 430 );
         });
      </script>
<? } else { ?>
<script src="/script_3.js"></script>
<style>
@media only screen and (max-width: 3000px) {
.menu_cont{width:100%; text-align:center; background-color:#303030; border-top:0px solid #FFFFFF; border-bottom:0px solid #FFFFFF;clear:both; display:inline-block;}
.menu_subcont{width:100%; max-width:1100px; margin:0px auto;}
.menu_menu{position: relative;width:100%; margin-left:0%;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  background: none;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 0px;
  background: #9c0000;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
  display:none;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul > li {
  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu ul li{
  font-size: 24px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
}
#cssmenu > ul > li > a {
  padding: 12px 7px 13px 7px;
  font-size: 26px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  background-color:#FFFFFF;
  color: #9c0000;
}
#cssmenu > ul > li.arrow {
  background-color:#303030;
  color: #FFFFFF;
  padding: 12px 0px 12px 0px;
  line-height:1;
  font-weight:normal;
  display:block;
}

#cssmenu > ul > li.nowactive a{
  color: #ff0000; 
}
#cssmenu > ul > li.nowactive a:hover{
  color: #9c0000; 
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 22px;
  right: 8px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  content: "";
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #9c0000;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 32px;
}
#cssmenu ul ul li a {
  padding: 10px 10px;
  width: 330px;
  font-family: 'Oswald', sans-serif;
  font-size: 19px;
  text-align:left;
  background: #FFFFFF;
  text-decoration: none;
  color: #303030;
  font-weight:400;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #303030;
  background-color:#FFFFFF;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 13px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
}

@media only screen and (max-width: 1024px) { 
.menu_cont{width:100%; text-align:center; background-color:#303030; border-top:0px solid #FFFFFF; border-bottom:0px solid #FFFFFF;clear:both; display:inline-block;}
.menu_subcont{width:100%; max-width:1100px; margin:0px auto;}
.menu_menu{position: relative;width:100%; margin-left:0%;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  background: none;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 0px;
  background: #9c0000;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
  display:none;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul > li {
  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu ul li{
  font-size: 24px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
}
#cssmenu > ul > li > a {
  padding: 12px 7px 9px 7px;
  font-size: 22px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  background-color:#FFFFFF;
  color: #9c0000;
}
#cssmenu > ul > li.arrow {
  background-color:#303030;
  color: #FFFFFF;
  padding: 8px 0px 8px 0px;
  line-height:1;
  font-weight:normal;
  display:block;
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 18px;
  right: 8px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  content: "";
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #9c0000;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 32px;
}
#cssmenu ul ul li a {
  padding: 10px 10px;
  width: 310px;
  font-family: 'Oswald', sans-serif;
  font-size: 18px;
  font-weight:400;
  text-align:left;
  background: #FFFFFF;
  text-decoration: none;
  color: #303030;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #303030;
}
#cssmenu > ul > li.arrow {
  background-color:#303030;
  color: #FFFFFF;
  padding: 8px 0px 8px 0px;
  line-height:1;
  font-weight:normal;
  display:none;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 13px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
}


@media only screen and (max-width: 768px){
.menu_cont{width:100%; text-align:center; background-color:#9c0000; border-top:3px solid #FFFFFF; border-bottom:3px solid #FFFFFF; height: auto;clear:both;}
.menu_subcont{width:100%; max-width:100%; margin:0px auto; height:auto;}
.menu_logo{position: relative ;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_logo img{width:30%; max-width:30%; height:auto; margin:0px auto; border:0px;padding:4px 0px 4px 0px;}
.menu_menu{position: relative;top: 0%;transform: none; float:none; width:100%; margin-left:0%;}
.menu_se_box{position: relative;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_aktion{color:#fffc00;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  background: none;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 3px;
  background: #9c0000;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul > li {

  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu > ul > li > a {
  padding: 10px;
  font-size: 15px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  color: #fffc00;
}
#cssmenu > ul > li.nowactive a{
  color: #ff0000; 
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 21px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  content: "";
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #fffc00;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 32px;
}
#cssmenu ul ul li a {
  padding: 10px 20px;
  width: 160px;
  font-size: 12px;
  background: #333333;
  text-decoration: none;
  color: #dddddd;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #ffffff;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 13px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
  #cssmenu {
    width: 100%;
	background:#FFFFFF;
  }
  #cssmenu ul {
    width: 100%;
    display: none;
  }
  #cssmenu.align-center > ul,
  #cssmenu.align-right ul ul {
    text-align: center;
  }
  #cssmenu ul li,
  #cssmenu ul ul li,
  #cssmenu ul li:hover > ul > li {
  	color:#000000;
	text-align:left;
	font-size:20px;
    width: 100%;
    height: auto;
    border-top: 1px dashed rgba(120, 120, 120, 0.45);
  }
  #cssmenu ul li a,
  #cssmenu ul ul li a {
  	color:#000000;
	font-size:20px;
    width: 100%;
  }
  #cssmenu ul li a:hover {
  	color:#9c0000;
  }
  #cssmenu > ul > li,
  #cssmenu.align-center > ul > li,
  #cssmenu.align-right > ul > li {
    float: none;
    display: block;
  }
  #cssmenu ul ul li a {
  	text-align:left;
    padding: 12px 20px 12px 20px;
	font-family: 'Poppins', sans-serif;
    font-size: 20px;
    color: #333333;
    background: none;
  }
  #cssmenu ul ul li:hover > a,
  #cssmenu ul ul li a:hover {
    color: #000000;
  }
  #cssmenu ul ul ul li a {
    padding-left: 40px;
  }
  #cssmenu ul ul,
  #cssmenu ul ul ul {
    position: relative;
    left: 0;
    right: auto;
    width: 100%;
    margin: 0;
  }
  #cssmenu > ul > li.has-sub > a::after,
  #cssmenu ul ul li.has-sub > a::after {
    display: none;
  }
  #menu-line {
    display: none;
  }
  #cssmenu #menu-button {
    display: block;
    padding: 18px;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 20px;
    text-transform: uppercase;
	background-color:#6c0b1c;
	text-align:left;
  }
  #cssmenu #menu-button::after {
    content: '';
    position: absolute;
    top: 20px;
    right: 20px;
    display: block;
    width: 15px;
    height: 2px;
    background: #FFFFFF;
  }
  #cssmenu #menu-button::before {
    content: '';
    position: absolute;
    top: 25px;
    right: 20px;
    display: block;
    width: 15px;
    height: 3px;
    border-top: 2px solid #FFFFFF;
    border-bottom: 2px solid #FFFFFF;
  }
  #cssmenu .submenu-button {
    position: absolute;
    z-index: 10;
    right: 0;
    top: 0;
    display: block;
    border-left: 1px solid rgba(120, 120, 120, 0.15);
    height: 42px;
    width: 52px;
    cursor: pointer;
  }
  #cssmenu .submenu-button::after {
    content: '';
    position: absolute;
    top: 16px;
    left: 22px;
    display: block;
    width: 1px;
    height: 11px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button::before {
    content: '';
    position: absolute;
    left: 17px;
    top: 21px;
    display: block;
    width: 11px;
    height: 1px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button.submenu-opened:after {
    display: none;
  }
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  color: #9c0000;
}
}

@media only screen and (max-width: 479px){
/* lila címke */
.menu_cont{width:100%; text-align:center; background-color:#6c0b1c; border-top:3px solid #FFFFFF; border-bottom:3px solid #FFFFFF; height: auto;clear:both;}
.menu_subcont{width:100%; max-width:100%; margin:0px auto; height:auto;}
.menu_logo{position: relative ;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_logo img{width:30%; max-width:30%; height:auto; margin:0px auto; border:0px;padding:4px 0px 4px 0px;}
.menu_menu{position: relative;top: 0%;transform: none; float:none; width:100%; margin-left:0%;}
.menu_se_box{position: relative;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_aktion{color:#fffc00;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  background: none;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 3px;
  background: #6c0b1c;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul > li {
  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu > ul > li > a {
  padding: 10px;
  font-size: 22px;
  text-decoration: none;
  text-transform: uppercase;
  color: #FFFFFF;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  color: #fffc00;
}
#cssmenu > ul > li.nowactive a{
  color: #ff0000; 
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 21px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  content: "";

  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #fffc00;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 32px;
}
#cssmenu ul ul li a {
  padding: 10px 20px;
  width: 160px;
  font-size: 12px;
  background: #333333;
  text-decoration: none;
  color: #dddddd;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #ffffff;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 13px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
  #cssmenu {
    width: 100%;
	background:#FFFFFF;
  }
  #cssmenu ul {
    width: 100%;
    display: none;
  }
  #cssmenu.align-center > ul,
  #cssmenu.align-right ul ul {
    text-align: center;
  }
  #cssmenu ul li,
  #cssmenu ul ul li,
  #cssmenu ul li:hover > ul > li {
  	color:#000000;
	text-align:left;
	font-size:20px;
    width: 100%;
    height: auto;
    border-top: 1px dashed rgba(120, 120, 120, 0.45);
  }
  #cssmenu ul li a,
  #cssmenu ul ul li a {
  	color:#000000;
    width: 100%;
  }
  #cssmenu ul li a:hover {
  	color:#9c0000;
  }
  #cssmenu > ul > li,
  #cssmenu.align-center > ul > li,
  #cssmenu.align-right > ul > li {
    float: none;
    display: block;
  }
  #cssmenu ul ul li a {
  	text-align:left;
    padding: 14px 20px 10px 15px;
	font-size:18px;
	font-family: 'Poppins', sans-serif;
    color: #333333;
    background: none;
  }
  #cssmenu ul ul li:hover > a,
  #cssmenu ul ul li a:hover {
    color: #000000;
  }
  #cssmenu ul ul ul li a {
    padding-left: 40px;
  }
  #cssmenu ul ul,
  #cssmenu ul ul ul {
    position: relative;
    left: 0;
    right: auto;
    width: 100%;
    margin: 0;
  }
  #cssmenu > ul > li.has-sub > a::after,
  #cssmenu ul ul li.has-sub > a::after {
    display: none;
  }
  #menu-line {
    display: none;
  }
  #cssmenu #menu-button {
    display: block;
    padding: 18px;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 20px;
    text-transform: uppercase;
	background-color:#6c0b1c;
	text-align:left;
  }
  #cssmenu #menu-button::after {
    content: '';
    position: absolute;
    top: 20px;
    right: 20px;
    display: block;
    width: 15px;
    height: 2px;
    background: #FFFFFF;
  }
  #cssmenu #menu-button::before {
    content: '';
    position: absolute;
    top: 25px;
    right: 20px;
    display: block;
    width: 15px;
    height: 3px;
    border-top: 2px solid #FFFFFF;
    border-bottom: 2px solid #FFFFFF;
  }
  #cssmenu .submenu-button {
    position: absolute;
    z-index: 10;
    right: 0;
    top: 0;
    display: block;
    border-left: 1px solid rgba(120, 120, 120, 0.15);
    height: 42px;
    width: 52px;
    cursor: pointer;
  }
  #cssmenu .submenu-button::after {
    content: '';
    position: absolute;
    top: 16px;
    left: 22px;
    display: block;
    width: 1px;
    height: 11px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button::before {
    content: '';
    position: absolute;
    left: 17px;
    top: 21px;
    display: block;
    width: 11px;
    height: 1px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button.submenu-opened:after {
    display: none;
  }
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  color: #9c0000;
}
}
</style>
<div class="menu_cont">
	<div class="menu_subcont">
        <div class="menu_menu" style="z-index:100;">
            <div id='cssmenu'>
                <ul>
                   <li><a href="/index.php">kezdőlap</a></li>
                   <li class="arrow">|</li>
                   <li><a href="/ferfi">férfi</a>
                      <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND termekek.Neme=1 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/ferfi/<?= $kat_nev_opt ?>" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>                   
                   </li>
                   <li class="arrow">|</li>
                   <li><a href="/noi">női</a>
                      <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND termekek.Neme=2 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/noi/<?= $kat_nev_opt ?>" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>  
                   </li>
                   <li class="arrow">|</li>
                   <li><a href="/gyermek">gyermek</a>
                      <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =1 AND termekek.Neme=3 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/gyermek/<?= $kat_nev_opt ?>" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>                     
                   </li>
                   <li class="arrow">|</li>
                   <li><a href="/kiegeszito">kiegészítők</a>
                      <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =2 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/kiegeszito/<?= $kat_nev_opt ?>" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>                     
                   </li>
                   <li class="arrow">|</li>
                   <li><a href="/textil">textil</a>
                      <ul>
<?
$query="SELECT DISTINCT termekek.Kategoria,kategoria.Nev,kategoria.nev_opt FROM termekek INNER JOIN kategoria ON termekek.Kategoria=kategoria.Id INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE termekek.Termek_tipus =5 AND termekek.Active=1 AND meret_ter.Darabszam > 0 $lekerd_kezdo_ebard_gyarto_idk ORDER BY kategoria.Nev ASC";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Kategoria");
$kat_nev=mysql_result($result,$i,"kategoria.Nev");
$kat_nev_opt=mysql_result($result,$i,"kategoria.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/textil/<?= $kat_nev_opt ?>" ><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>                     
                   </li>
                   <li class="arrow">|</li>
                   <li><a href="index.php?mod=gyrt">márkák</a>
                   
                      <ul>
<?
$query="SELECT DISTINCT termekek.Gyarto,gyarto.Nev,gyarto.nev_opt FROM termekek INNER JOIN termek_kepek ON termekek.Id=termek_kepek.Gal_id INNER JOIN gyarto ON termekek.Gyarto=gyarto.Id INNER JOIN meret_ter ON termekek.Id=meret_ter.Ter_id WHERE  meret_ter.Darabszam > 0 AND termekek.Active=1 $lekerd_kezdo_ebard_gyarto_idk ORDER BY gyarto.Nev ASC ";
//echo $query;
$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
while ($i < $num) {
$kategoria=mysql_result($result,$i,"termekek.Gyarto");
$kat_nev=mysql_result($result,$i,"gyarto.Nev");
$gyarto_nev_opt=mysql_result($result,$i,"gyarto.nev_opt");
if($kat_nev != ""){ ?>
                         <li><a href="/<?= $gyarto_nev_opt ?>"><?= $kat_nev ?></a></li>
<? }
$i++;
}
?>
                      </ul>                     
                   
                   </li>
                   <li class="arrow">|</li>
                   <li ><a href="/orias-termekek">Óriás termékek</a></li>
                   <li class="arrow">|</li>
                   <li ><a href="/brand-club">ebrand club</a></li>
                   <li class="arrow">|</li>
                   <li class="nowactive"><a href="/termekcsere-garancia/">termékcsere, garancia</a></li>
                   <!--<li class="active"><a href='#'>most akcióban %</a></li>-->
                </ul>
            </div>        
        </div>

    </div>
</div>
<? } ?>