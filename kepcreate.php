<?
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");




$rootPath = realpath('termek_pics');

// Initialize archive object
$zip = new ZipArchive();
$zip->open('feeds/img_files.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();

?>

<div class="inside_cont_all_cont">
    <div class="inside_cont_all_cont_incont_nomarg inside_cont_all_cont_incont_nomarg_height_40">
    	<div class="allcont_wheris_cont"><a href="/">Kezdőlap</a> > Zip</div>
    </div>
    
    <div class="inside_cont_all_cont_incont_nomarg inside_cont_all_cont_incont_nomarg_mrgtopbtn20">
    	<div class="kateg_divider_text_cont_ins">
			<div class="knyvar_allsite_text"><a href="/feeds/img_files.zip" target="_blank">Zip file letöltése</a></div>
        </div>
    </div>
</div>