<? 
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");
?>
<script>
<? if(!isset($fomod)) { ?>
var melyik_galt_nezze = 0;
<? } else if($fomod=="klsz") { ?>
var melyik_galt_nezze = 1;
<? } ?>
var galholvan = 1;
var kepszamlalo = 1;
var prev_pic = 0;

var akt_pic = 0;

</script>

<style>
@media only screen and (max-width: 3000px) {
#promoboxes_cont{width:100%; border-bottom:0px solid #FFFFFF;border-top:0px solid #FFFFFF; display:inline-block;position: relative; text-align:center; z-index:0;}
#kep_box_0{ position:absolute; margin:0px; padding:0;width:100%; height:400px; z-index:0; display:block;}
#kep_box_1{ position:absolute; margin:0px; padding:0;width:100%; height:400px; z-index:1; display:block;}
#kep_allboxes{width:100%; height:400px; text-align:center; display:inline-block; background-color:#FFFFFF;}
.gal_next_arrow{position:absolute; right:10px;width:38px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:20; cursor:pointer; top: 86%; -webkit-transform: translateY(-86%);-ms-transform: translateY(-86%);transform: translateY(-86%);}
.gal_prev_arrow{position:absolute;width:38px;right:53px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:21; cursor:pointer;top: 86%; -webkit-transform: translateY(-86%);-ms-transform: translateY(-86%);transform: translateY(-86%); }

.gal_next_arrow img{margin:0px 0px 0px 0px; width:76px; max-width:76px; height:auto; cursor:pointer; border:0px;}
#gal_next_arrow_img{-webkit-transition: all 0.15s ease;-moz-transition: all 0.15s ease;-o-transition: all 0.15s ease;transition: all 0.15s ease;}
#gal_next_arrow_img:hover{ margin-left:-100%;}

.gal_prev_arrow img{margin:0px 0px 0px 0px; width:76px; max-width:76px; height:auto; cursor:pointer; border:0px;}
#gal_prev_arrow_img{-webkit-transition: all 0.15s ease;-moz-transition: all 0.15s ease;-o-transition: all 0.15s ease;transition: all 0.15s ease;}
#gal_prev_arrow_img:hover{ margin-left:-100%;}
.gal_button_all_cont{position:absolute; z-index:10; right:25px; top:2%; display:block;}
.gal_button_count{
	font-family: 'Poppins', sans-serif;
    font-size: 14px;
	color:#FFFFFF;
	background: rgba(64, 96, 80, 0.85);
	text-decoration:none;
    -moz-border-radius:100%;
    -webkit-border-radius:100%;
	border-radius:100%;	
	text-align:center;
	padding:0px 0px 0px 0px;
	border:1px solid #608070;
	cursor:pointer;
	font-weight:bold;
	text-transform:uppercase;
	width:30px;
	height:30px;
	-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;
}
.gal_button_count:hover{
	color:#608070;
	background-color:#FFFFFF;
	border:1px solid #608070;
}

.gal_button_count_up{
	font-family: 'Poppins', sans-serif;
    font-size: 14px;
	color:#608070;
	background: rgba(255, 255, 255, 0.85);
	text-decoration:none;
    -moz-border-radius:100%;
    -webkit-border-radius:100%;
	border-radius:100%;	
	text-align:center;
	padding:0px 0px 0px 0px;
	border:1px solid #608070;
	cursor:pointer;
	font-weight:bold;
	width:30px;
	height:30px;
	text-transform:uppercase;
	-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;
}
.gal_button_count_up:hover{
	color:#608070;
	background-color:#FFFFFF;
	border:1px solid #608070;
}
.gal_buttons_items{margin-left:5px; float:left;}
#promo_text_block{position:absolute; width:100%; text-align:center; z-index:14; bottom:8%; display:inline-block;}
.gal_text_block_sub{width:100%; max-width:100%; margin:0px auto; text-align:left; }
.gal_text_block_text_cont{width:70%; display:inline-block; left:0; background: -webkit-linear-gradient(left, rgba(110,182,110, 0.8) , rgba(0, 0, 0, 0.0));background: -o-linear-gradient(right, rgba(110,182,110, 0.8) , rgba(0, 0, 0, 0.0));background: -moz-linear-gradient(right, rgba(110,182,110, 0.8) , rgba(0, 0, 0, 0.0)); background: linear-gradient(to right, rgba(110,182,110, 0.8) , rgba(0, 0, 0, 0.0)); margin:0px 0px 0px 5%;}
.gal_text_block_text_cont_ins{width:60%; left:0; padding:7px 0px 14px 10px;font-family: 'Poppins', sans-serif; font-size:37px; line-height:40px; letter-spacing:0.5px; color:#FFFFFF; text-transform:uppercase; font-weight:500;}
.gal_text_block_text_cont_ins span{font-family: 'Poppins', sans-serif;font-size:26px; line-height:30px; color:#FFFFFF; text-transform:none; font-weight:300;letter-spacing:0px; }

.text_button{
	margin-top:14px;
	font-family: 'Poppins', sans-serif;
    font-size: 16px;
	color:#FFFFFF;
	background: transparent;
	text-decoration:none;
    -moz-border-radius:0px;
    -webkit-border-radius:0px;
	border-radius:0px 0px 0px 0px;	
	text-align:center;
	padding:5px 22px 5px 22px;
	border:1px solid #FFFFFF;
	cursor:pointer;
	font-weight:400;
	text-transform:uppercase;
	-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;
}
.text_button:hover{
	color:#2d5e81;
	background-color:#FFFFFF;
	border:1px solid #FFFFFF;
}
.ad_ins_block_abs{ width:20%; max-width:20%; position:absolute; z-index:10203; display:inline-block; text-align:left; border:0px solid #930; left:0px;}
.ad_ins_block_abs img{ width:100%; max-width:100%; height:auto; border:0px solid #930; margin:0px auto;}
}

@media only screen and (max-width: 1024px) { 
#kep_box_0{ position:absolute; margin:0px; padding:0;width:100%; height:400px; z-index:0; display:block;}
#kep_box_1{ position:absolute; margin:0px; padding:0;width:100%; height:400px; z-index:1; display:block;}
#kep_allboxes{width:100%; height:400px; text-align:center; display:inline-block; background-color:#FFFFFF;}
.gal_text_block_text_cont_ins{width:90%; left:0; padding:7px 0px 14px 10px;font-family: 'Poppins', sans-serif; font-size:22px; line-height:28px; letter-spacing:0.5px; color:#FFFFFF; text-transform:uppercase; font-weight:700;}
.gal_text_block_text_cont_ins span{font-size:16px; line-height:20px; color:#FFFFFF; text-transform:none; font-weight:400;letter-spacing:0px; }
.gal_next_arrow{position:absolute; right:10px;width:38px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:20; cursor:pointer; top: 90%; -webkit-transform: translateY(-90%);-ms-transform: translateY(-90%);transform: translateY(-90%);}
.gal_prev_arrow{position:absolute;width:38px; right:53px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:21; cursor:pointer;top: 90%; -webkit-transform: translateY(-90%);-ms-transform: translateY(-90%);transform: translateY(-90%); }
}


@media only screen and (max-width: 768px){
#kep_box_0{ position:absolute; margin:0px; padding:0;width:100%; height:300px; z-index:0; display:block;}
#kep_box_1{ position:absolute; margin:0px; padding:0;width:100%; height:300px; z-index:1; display:block;}
#kep_allboxes{width:100%; height:300px; text-align:center; display:inline-block; background-color:#FFFFFF;}
#promo_text_block{position:absolute; width:100%; text-align:center; z-index:14; bottom:5%; display:inline-block;}
.gal_text_block_text_cont_ins{width:90%; left:0; padding:7px 0px 14px 10px;font-family: 'Poppins', sans-serif; font-size:19px; line-height:24px; letter-spacing:0.5px; color:#FFFFFF; text-transform:uppercase; font-weight:700;}
.gal_text_block_text_cont_ins span{font-size:14px; line-height:17px; color:#FFFFFF; text-transform:none; font-weight:400;letter-spacing:0px; }

.text_button{
	margin-top:10px;
	font-family: 'Poppins', sans-serif;
    font-size: 14px;
	color:#FFFFFF;
	background: transparent;
	text-decoration:none;
    -moz-border-radius:0px;
    -webkit-border-radius:0px;
	border-radius:0px 0px 0px 0px;	
	text-align:center;
	padding:3px 12px 3px 12px;
	border:1px solid #FFFFFF;
	cursor:pointer;
	font-weight:400;
	text-transform:uppercase;
	-webkit-transition: 0.2s ease-in-out all;-moz-transition: 0.2s ease-in-out all;-o-transition: 0.2s ease-in-out all;transition: 0.2s ease-in-out all;
}
.text_button:hover{
	color:#0b4a80;
	background-color:#FFFFFF;
	border:1px solid #FFFFFF;
}
.gal_button_all_cont{position:absolute; z-index:10; left:0px; top:5%; display:none;}
.gal_next_arrow{position:absolute; right:0px;width:38px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:20; cursor:pointer; top: 50%; -webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);}
.gal_prev_arrow{position:absolute;width:38px; right:auto; left:0px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:21; cursor:pointer;top: 50%; -webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%); }
.ad_ins_block_abs{ width:30%; max-width:30%; position:absolute; z-index:10203; display:inline-block; text-align:left; border:0px solid #930; left:0px;}
}

@media only screen and (max-width: 479px){
#kep_box_0{ position:absolute; margin:0px; padding:0;width:100%; height:200px; z-index:0; display:block;}
#kep_box_1{ position:absolute; margin:0px; padding:0;width:100%; height:200px; z-index:1; display:block;}
#kep_allboxes{width:100%; height:200px; text-align:center; display:inline-block; background-color:#FFFFFF;}
.gal_next_arrow{position:absolute; right:0px;width:38px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:20; cursor:pointer; top: 10%; -webkit-transform: translateY(-10%);-ms-transform: translateY(-10%);transform: translateY(-10%);}
.gal_prev_arrow{position:absolute;width:38px; right:auto; left:0px; overflow:hidden; border-top:0px solid #a01c20;border-right:0px solid #a01c20; border-bottom:0px solid #a01c20;margin:0px 0px 0px 0px; z-index:21; cursor:pointer;top: 10%; -webkit-transform: translateY(-10%);-ms-transform: translateY(-10%);transform: translateY(-10%); }
}



</style>
	<div id="promoboxes_cont">
<?



				//$query="SELECT Id,File_name,Extend,Url,Aktiv,Tipus,Forraskod,Szeles,Magas,Nev FROM banner WHERE Aktiv = 1 AND Ban_hely = 32 AND NOW()>Mettol AND NOW()<Meddig ORDER BY RAND() LIMIT 1";
				$query="SELECT Id,File_name,Extend,Url,Aktiv,Tipus,Forraskod,Szeles,Magas,Nev FROM banner WHERE Aktiv = 1 AND Ban_hely = 34 AND NOW()>Mettol AND NOW()<Meddig ORDER BY RAND() LIMIT 1";
				//echo $query;
				$result=mysql_query($query);
				$num=mysql_numrows($result);
				
				if($num > 0){ 
				?>
                <div class="ad_ins_block_abs">
                <?
					$i=0;
					while ($i < $num) {
						$Id=mysql_result($result,$i,"Id");
						$file_name=mysql_result($result,$i,"File_name");
						$extend=mysql_result($result,$i,"Extend");
						$url=mysql_result($result,$i,"Url");
						$tipus=mysql_result($result,$i,"Tipus");
						$forraskod=mysql_result($result,$i,"Forraskod");
						$szeles=mysql_result($result,$i,"Szeles");
						$magas=mysql_result($result,$i,"Magas");
						$banner_neve=mysql_result($result,$i,"Nev");
						$seo_link_banner_neve=magyarkarakter($banner_neve);
						$query_nezve = "UPDATE banner SET Megjelent=Megjelent+1 WHERE Id=$Id ";
						mysql_query($query_nezve);
						
						if($tipus == 0){
						
						if($extend == "swf"){ ?>
                            <script type="text/javascript">
                                                    AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','<?= $szeles ?>','height','<?= $magas ?>','src','/banner/<?= $file_name ?>','quality','high','wmode','transparent','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','/banner/<?= $file_name ?>' ); //end AC code
                                                    </script>
						<? } else { 
						$filename = "banners/".$file_name."";
						 
						list($width, $height) = getimagesize($filename);
						
						?>
                        <!--<div style="width:1000px; margin-top:6px;"><img src="/images/banner_1000px.jpg" /></div>-->
                        <a href="http://<?= $url ?>" target="_blank"><img src="/<?= $filename ?>" alt="" /></a>
                        <!--<a href="/bannerkatt/<?= $Id ?>-<?= $seo_link_banner_neve ?>" target="_blank"><img src="/<?= $filename ?>" alt="" /></a>-->
						<? }
						} else { ?>
                        <?= $forraskod ?>
						<? } 

					$i++;
					} 				
			
				?>
                
			</div>

<?	} 

?>  

		<div class="gal_button_all_cont">
<? 



if(!isset($fomod)) {
	$lekerd_promogal = " WHERE Melyik_oldal=0 AND Nyelv = '$_SESSION[nyelv]'";
	$lekerd_promogal_and = " AND Melyik_oldal=0 ";
} else if($fomod=="klsz") {
	$lekerd_promogal = " WHERE Melyik_oldal=1 AND Nyelv = '$_SESSION[nyelv]'";
	$lekerd_promogal_and = " AND Melyik_oldal=1";
}

$result = mysql_query("SELECT COUNT(Id) AS osszidk FROM kezdo_promo $lekerd_promogal");
$row = mysql_fetch_assoc($result);
$osszidk = $row['osszidk'];
//echo $osszidk;

if($osszidk > 1){
$query="SELECT Id FROM kezdo_promo $lekerd_promogal ORDER BY Helyezes ASC";

$result=mysql_query($query);
$num=mysql_numrows($result);
$i=0;
$z=1;
while ($i < $num) {
$Id=mysql_result($result,$i,"Id");
?>

	<div class="gal_buttons_items" id="gal_button_<?= $i ?>"><button class="gal_button_count" id="gal_button_ka_<?= $i ?>" onclick="gal_kap_ch(<?= $i ?>)"><?= $z ?></button></div>

<?
$z++;
 
$i++;
}
}
?>
		</div>
<?
$result = mysql_query("SELECT Id,Helyezes,Link,Also_szoveg,Felso_szoveg,Kepnev,Felso_szoveg_en,Also_szoveg_en,Button_name,Button_name_en,Felirat,Felso_szoveg_de,Also_szoveg_de,Button_name_de FROM kezdo_promo $lekerd_promogal ORDER BY Helyezes ASC LIMIT 1");
$row = mysql_fetch_assoc($result);
$kezdo_id = $row['Id'];
$kep_helyezes = $row['Helyezes'];
$kep_link = $row['Link'];
/*if($_SESSION['nyelv']=="hu"){
	$also_szoveg = $row['Also_szoveg'];
	$felso_szoveg = $row['Felso_szoveg'];
	$btn_name = $row['Button_name'];
} else if($_SESSION['nyelv']=="en"){
	$also_szoveg = $row['Also_szoveg_en'];
	$felso_szoveg = $row['Felso_szoveg_en'];
	$btn_name = $row['Button_name_en'];	
} else {
	$also_szoveg = $row['Also_szoveg_de'];
	$felso_szoveg = $row['Felso_szoveg_de'];
	$btn_name = $row['Button_name_de'];		
}*/
	$also_szoveg = $row['Also_szoveg'];
	$felso_szoveg = $row['Felso_szoveg'];
	$btn_name = $row['Button_name'];
$kepnev = $row['Kepnev'];
$felirat = $row['Felirat'];

$result = mysql_query("SELECT Kepnev FROM kezdo_promo WHERE Helyezes > $kep_helyezes $lekerd_promogal_and ORDER BY Helyezes ASC LIMIT 1");
$row = mysql_fetch_assoc($result);
$next_pic = $row['Kepnev'];

$result = mysql_query("SELECT COUNT(Id) AS osszkepszam FROM kezdo_promo $lekerd_promogal");
$row = mysql_fetch_assoc($result);
$osszkepszam = $row['osszkepszam'];

?> 
		<? if($osszidk > 1){ ?>     
    	<div class="gal_prev_arrow" onClick="gal_prev_pic()"><img src="/images/arrow_lft.png" id="gal_prev_arrow_img"></div>
    	<div class="gal_next_arrow" onClick="gal_next_pic_next()"><img src="/images/arrow_rgt.png" id="gal_next_arrow_img"></div>
        <? } ?>
        <div id="promo_text_block">
        	<div class="gal_text_block_sub">
            	<div class="gal_text_block_text_cont">
            		<div class="gal_text_block_text_cont_ins" id="promo_text_cont"><?= $felso_szoveg ?><br /><span><?= $also_szoveg ?></span><br /><a href="http://<?= $kep_link ?>"><button class="text_button"><?= $btn_name ?></button></a></div>
                </div>
            </div>
        </div>

<script>
var osszkepszam = <?= $osszkepszam ?>;
<? if($felirat == 0){ ?>
	$("#promo_text_block").css("opacity",0);
<? } ?>
</script>
	<div id="kep_allboxes">
	<a href="http://<?= $kep_link ?>" id="kep_box_1_link"><div style="background: url(/promo_pics/<?= $kepnev ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;  -o-background-size: cover;background-size: cover;" id="kep_box_1">&nbsp;</div></a>
		<div style="background: url(/promo_pics/<?= $next_pic ?>) no-repeat center center;-webkit-background-size: cover;-moz-background-size: cover;  -o-background-size: cover;background-size: cover;" id="kep_box_0">&nbsp;</div>
	</div>
    




    
   
</div>



<script>
$("#gal_button_ka_0").removeClass("gal_button_count");
$("#gal_button_ka_0").addClass("gal_button_count_up");

function gal_kap_ch(num){
	//$("#teszt").append("Gal holvan: "+akt_pic+"");
			clearTimeout(t);
			$( "#promo_text_block" ).animate({
				opacity: 0
			}, 400, function() {
				$("#promo_text_block").css("display", "none");
				$('#promo_text_cont').html("");
			});	
			//alert(galholvan);
			var request = $.ajax({
				url: "/ax_gal_katt_pic.php",
				//data: {},
				data: {'t_id': akt_pic,'sum_id': osszkepszam,'akt_id': num,'mely_gal':melyik_galt_nezze},
				type: "GET",           
				dataType: "html"
			});
	 
			request.done(function(msg) {
				myArr = msg.split("%/");
				akt_id = eval(myArr[0]);
				u_id = eval(myArr[1]);
				pichely = eval(myArr[2]);
				next_pic = myArr[3];
				prev_pic = myArr[4];
				akt_pic = eval(myArr[5]);
				visszahtml = myArr[6];
				felirat = eval(myArr[7]);
				kep_link = myArr[8];
				//alert(akt_id);
				document.getElementById("kep_box_1_link").href = "https://"+kep_link;
				if(u_id == 1){
					if(felirat == 1){
						$( "#promo_text_block" ).animate({
							opacity: 1
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
					} else {
						$( "#promo_text_block" ).animate({
							opacity: 0
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
						
					}	
					for (f = 0; f < osszkepszam; f++) {
						$("#gal_button_ka_"+f+"").removeClass("gal_button_count");
						$("#gal_button_ka_"+f+"").removeClass("gal_button_count_up");
						$("#gal_button_ka_"+f+"").addClass("gal_button_count");
						//$("#gal_button_"+f+"").css("background", "rgba(64, 96, 80, 0.85)");
					}
					$("#gal_button_ka_"+akt_pic+"").addClass("gal_button_count_up");

					$('#kep_box_0').css("background", "url(/promo_pics/"+prev_pic+")  no-repeat center center");
					$('#kep_box_0').css("-webkit-background-size", "cover");
					$('#kep_box_0').css("-moz-background-size", "cover");
					$('#kep_box_0').css("-o-background-size", "cover");
					$('#kep_box_0').css("background-size", "cover");					


						  $( "#kep_box_1" ).animate({
							opacity: 0
						  }, 400, function() {
							$('#kep_box_1').css("background", "url(/promo_pics/"+next_pic+")  no-repeat center center");
							$('#kep_box_1').css("-webkit-background-size", "cover");
							$('#kep_box_1').css("-moz-background-size", "cover");
							$('#kep_box_1').css("-o-background-size", "cover");
							$('#kep_box_1').css("background-size", "cover");
							  $("#kep_box_1").animate({
								opacity: 1
							  }, 400 );	
						  });
						if(num == osszkepszam-1){
							galholvan = 0;
						} else {
							galholvan = num+1;
						}

				} 
			});
	 
			request.fail(function(jqXHR, textStatus) {
				//alert( "Request failed: " + textStatus );
			});	
}

function gal_next_pic_next(){
			clearTimeout(t);	

			$( "#promo_text_block" ).animate({
				opacity: 0
			}, 400, function() {
				$("#promo_text_block").css("display", "none");
				$('#promo_text_cont').html("");
			});			

			//alert(galholvan);
			var request = $.ajax({
				url: "/ax_gal_next_pic.php",
				//data: {},
				data: {'t_id': galholvan,'sum_id': osszkepszam,'mely_gal':melyik_galt_nezze},
				type: "GET",           
				dataType: "html"
			});
	 
			request.done(function(msg) {
				myArr = msg.split("%/");
				akt_id = eval(myArr[0]);
				u_id = eval(myArr[1]);
				pichely = eval(myArr[2]);
				next_pic = myArr[3];
				prev_pic = myArr[4];
				akt_pic = eval(myArr[5]);
				visszahtml = myArr[6];
				felirat = eval(myArr[7]);
				kep_link = myArr[8];
				//array_count = myArr[2];
				//alert(felirat);
				document.getElementById("kep_box_1_link").href = "https://"+kep_link;
				if(u_id == 1){
					if(felirat == 1){
						$( "#promo_text_block" ).animate({
							opacity: 1
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
					} else {
						$( "#promo_text_block" ).animate({
							opacity: 0
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
						
					}
					//$("#teszt").append("Gal holvan: "+galholvan+"");
				for (f = 0; f < osszkepszam; f++) {
					$("#gal_button_ka_"+f+"").removeClass("gal_button_count");
					$("#gal_button_ka_"+f+"").removeClass("gal_button_count_up");
					$("#gal_button_ka_"+f+"").addClass("gal_button_count");
					//$("#gal_button_"+f+"").css("background", "rgba(64, 96, 80, 0.85)");
				}
				$("#gal_button_ka_"+akt_pic+"").addClass("gal_button_count_up");

					$('#kep_box_0').css("background", "url(/promo_pics/"+prev_pic+")  no-repeat center center");
					$('#kep_box_0').css("-webkit-background-size", "cover");
					$('#kep_box_0').css("-moz-background-size", "cover");
					$('#kep_box_0').css("-o-background-size", "cover");
					$('#kep_box_0').css("background-size", "cover");					
					

						  $( "#kep_box_1" ).animate({
							opacity: 0
						  }, 400, function() {
							$('#kep_box_1').css("background", "url(/promo_pics/"+next_pic+")  no-repeat center center");
							$('#kep_box_1').css("-webkit-background-size", "cover");
							$('#kep_box_1').css("-moz-background-size", "cover");
							$('#kep_box_1').css("-o-background-size", "cover");
							$('#kep_box_1').css("background-size", "cover");
							  $("#kep_box_1").animate({
								opacity: 1
							  }, 400 );	
						  });
						if(galholvan < osszkepszam-1){
							galholvan++;
							
						} else {
							galholvan = 0;
							
						}
				} 
			});
	 
			request.fail(function(jqXHR, textStatus) {
				//alert( "Request failed: " + textStatus );
			});

}


function gal_prev_pic(){
			//alert(kepminusz);
			clearTimeout(t);
			$( "#promo_text_block" ).animate({
				opacity: 0
			}, 400, function() {
				$("#promo_text_block").css("display", "none");
				$('#promo_text_cont').html("");
			});	
			if(galholvan == 0){
				galholvan = osszkepszam - 1;
			} else {
				galholvan--;
			}

			
			var request = $.ajax({
				url: "/ax_gal_prev_pic.php",
				//data: {},
				data: {'t_id': galholvan,'sum_id': osszkepszam,'mely_gal':melyik_galt_nezze},
				type: "GET",           
				dataType: "html"
			});
	 
			request.done(function(msg) {
				myArr = msg.split("%/");
				akt_id = eval(myArr[0]);
				u_id = eval(myArr[1]);
				pichely = eval(myArr[2]);
				next_pic = myArr[3];
				prev_pic = myArr[4];
				akt_pic = eval(myArr[5]);
				visszahtml = myArr[6];
				felirat = eval(myArr[7]);
				kep_link = myArr[8];
				//array_count = myArr[2];
				document.getElementById("kep_box_1_link").href = "https://"+kep_link;
				if(u_id == 1){
					if(felirat == 1){
						$( "#promo_text_block" ).animate({
							opacity: 1
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
					} else {
						$( "#promo_text_block" ).animate({
							opacity: 0
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
						
					}	
					for (f = 0; f < osszkepszam; f++) {
						$("#gal_button_ka_"+f+"").removeClass("gal_button_count");
						$("#gal_button_ka_"+f+"").removeClass("gal_button_count_up");
						$("#gal_button_ka_"+f+"").addClass("gal_button_count");
						//$("#gal_button_"+f+"").css("background", "rgba(64, 96, 80, 0.85)");
					}
				$("#gal_button_ka_"+akt_pic+"").addClass("gal_button_count_up");
					/*$("#teszt").append("Gal holvan: "+galholvan+"");
					$("#teszt").append("Előző kép: "+prev_pic+"");
					$("#teszt").append("Aktuális kép: "+next_pic+"");*/
					$('#kep_box_0').css("background", "url(/promo_pics/"+prev_pic+")  no-repeat center center");
					$('#kep_box_0').css("-webkit-background-size", "cover");
					$('#kep_box_0').css("-moz-background-size", "cover");
					$('#kep_box_0').css("-o-background-size", "cover");
					$('#kep_box_0').css("background-size", "cover");					


						  $( "#kep_box_1" ).animate({
							opacity: 0
						  }, 400, function() {
							$('#kep_box_1').css("background", "url(/promo_pics/"+next_pic+")  no-repeat center center");
							$('#kep_box_1').css("-webkit-background-size", "cover");
							$('#kep_box_1').css("-moz-background-size", "cover");
							$('#kep_box_1').css("-o-background-size", "cover");
							$('#kep_box_1').css("background-size", "cover");
							  $("#kep_box_1").animate({
								opacity: 1
							  }, 400 );	
						  });

				} 
			});
	 
			request.fail(function(jqXHR, textStatus) {
				//alert( "Request failed: " + textStatus );
			});

}

<? if($osszidk > 1){ ?>
var t=setTimeout("gal_next_pic()",4000);
<? } ?>






function gal_next_pic(){
			$( "#promo_text_block" ).animate({
				opacity: 0
			}, 400, function() {
				$("#promo_text_block").css("display", "none");
				$('#promo_text_cont').html("");
			});	
			//alert(galholvan);
			var request = $.ajax({
				url: "/ax_gal_next_pic.php",
				//data: {},
				data: {'t_id': galholvan,'sum_id': osszkepszam,'mely_gal':melyik_galt_nezze},
				type: "GET",           
				dataType: "html"
			});
	 
			request.done(function(msg) {
				myArr = msg.split("%/");
				akt_id = eval(myArr[0]);
				u_id = eval(myArr[1]);
				pichely = eval(myArr[2]);
				next_pic = myArr[3];
				prev_pic = myArr[4];
				akt_pic = eval(myArr[5]);
				visszahtml = myArr[6];
				felirat = eval(myArr[7]);
				kep_link = myArr[8];
				document.getElementById("kep_box_1_link").href = "https://"+kep_link;
				for (f = 0; f < osszkepszam; f++) {
					$("#gal_button_ka_"+f+"").removeClass("gal_button_count");
					$("#gal_button_ka_"+f+"").removeClass("gal_button_count_up");
					$("#gal_button_ka_"+f+"").addClass("gal_button_count");
					//$("#gal_button_"+f+"").css("background", "rgba(64, 96, 80, 0.85)");
				}
				$("#gal_button_ka_"+akt_pic+"").addClass("gal_button_count_up");
				//$("#gal_button_"+akt_pic+"").css("background", "rgba(100, 255, 255, 1)");

				if(u_id == 1){
					if(felirat == 1){
						$( "#promo_text_block" ).animate({
							opacity: 1
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
					} else {
						$( "#promo_text_block" ).animate({
							opacity: 0
						}, 600, function() {
							$("#promo_text_block").css("display", "block");
							$("#promo_text_cont").html(visszahtml);								
						});
						
					}

					$('#kep_box_0').css("background", "url(/promo_pics/"+prev_pic+")  no-repeat center center");
					$('#kep_box_0').css("-webkit-background-size", "cover");
					$('#kep_box_0').css("-moz-background-size", "cover");
					$('#kep_box_0').css("-o-background-size", "cover");
					$('#kep_box_0').css("background-size", "cover");					


						  $( "#kep_box_1" ).animate({
							opacity: 0
						  }, 400, function() {
							$('#kep_box_1').css("background", "url(/promo_pics/"+next_pic+")  no-repeat center center");
							$('#kep_box_1').css("-webkit-background-size", "cover");
							$('#kep_box_1').css("-moz-background-size", "cover");
							$('#kep_box_1').css("-o-background-size", "cover");
							$('#kep_box_1').css("background-size", "cover");
							  $("#kep_box_1").animate({
								opacity: 1
							  }, 400 );	
						  });
						if(galholvan < osszkepszam-1){
							galholvan++;
							
						} else {
							galholvan = 0;
							
						}
				t=setTimeout("gal_next_pic()",4000);
				} 

			});
	 
			request.fail(function(jqXHR, textStatus) {
				//alert( "Request failed: " + textStatus );
			});

}
//t=setTimeout("gal_next_pic()",3000);




var magas = $(window).innerHeight();
var szeles = $(window).width();



$( document ).ready(function() {
	/*$("#kep_box_0").height(myHeight);
	$("#kep_box_1").height(myHeight);
	$("#kep_allboxes").height(myHeight);
	$("#promoboxes_cont").height(myHeight);*/
});

$(window).resize(function(){
  /*var myWidth = 0, myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }
	$("#kep_box_0").height(myHeight);
	$("#kep_box_1").height(myHeight);
	$("#kep_allboxes").height(myHeight);
	$("#promoboxes_cont").height(myHeight);*/
});
</script>