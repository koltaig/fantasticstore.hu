<?
//ez dönti el az url paraméterek alapján hogy melyik aloldalt töltjük majd be, egy mod paraméter beállításával, amit később az index.php felhasznál
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");

function magyarkarakter($str) {
		$ekezet= array("-","/","&#8482;","?","Ö","ö","Ü","ü","ű","Ó","ó","O","o","Ú","ú","Á","á","U","u","É","é","Í","í"," ","+","'","ő", "Ű", "Ő", "ä","Ä","ű","Ű","ő","Ő"," ",",","!","?",":","/","(",")","","š","ę","","č","ň","","","Č","","ô","ý","ä","ž","d'","ł","ë","â","ź","","ć","ě","ř","ń","Ł","","Ô","ň","<",">","#","@",".","„","”");
		$nekezet=array("","","","","O","o","U","u","u","O","o","O","o","U","u","A","a","U","u","E","e","I","i","-","-","-","o", "U", "O", "a","A","u","u","o","o","","","","","","","","","s","a","e","S","c","n","z","s","C","Z","o","y","a","l","d","L","e","a","L","t","c","e","r","n","L","s","O","n","","","","","","","");
		return(strtolower(str_replace($ekezet,$nekezet,$str)));
}
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}   
   
function parse_path() {
  $path = array();
  if (isset($_SERVER['REQUEST_URI'])) {
	  $rest = substr($_SERVER['REQUEST_URI'], -1); 
	if($rest == "/"){
		$all_url = $_SERVER['REQUEST_URI'];
		$all_url = substr($all_url, 0, -1);	
	} else {
	$all_url = $_SERVER['REQUEST_URI'];
	}
    $request_path = explode('?', $all_url);

    $path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
    $path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);
    $path['call'] = utf8_decode($path['call_utf8']);
    if ($path['call'] == basename($_SERVER['PHP_SELF'])) {
      $path['call'] = '';
    }
    $path['call_parts'] = explode('/', $path['call']);

    $path['query_utf8'] = urldecode($request_path[1]);
    $path['query'] = utf8_decode(urldecode($request_path[1]));
    $vars = explode('&', $path['query']);
    foreach ($vars as $var) {
      $t = explode('=', $var);
      $path['query_vars'][$t[0]] = $t[1];
    }
  }
return $path;
}



$path_info = parse_path();
$array_count = count($path_info['call_parts']);
$cat_alap = $path_info['call_parts'][0];
$cat_alap_2 = $path_info['call_parts'][1];
$cat_alap_3 = $path_info['call_parts'][2];
//print_r($array_count);
//print_r($path_info['call_parts'][0]);
//print_r($path_info['call_parts']);

if($cat_alap != ""){

	if($cat_alap == "akciok"){		
		include("optika_for_search.php");
		//$tdebug.="(akciok_search)";
		$mod = "akciok_se";
		$all_site_title .= " - Akciós termékek";
		//echo "AAA";
	} else if($array_count == 1){
		//$tdebug.="(1)";		
		if($cat_alap == "shop"){
			$mod = "se";
			$all_site_title = "Fantastic store - Termékek"; 
/*			
		} else if($cat_alap == "akciok"){		
			$mod = "akciok";
			$all_site_title = "Fantastic store - Akciós termékek";
			//echo "AAA";
*/
		} else if($cat_alap == "kosar"){
			$mod = "ks";
			$all_site_title = "Fantastic store - Kosár tartalma"; 
		} else if($cat_alap == "regisztracio"){
			$mod = "reg";
			$all_site_title = "Fantastic store - Regisztráció"; 
		} else if($cat_alap == "cegregisztracio"){
			$mod = "creg";
			$all_site_title = "Fantastic store - Céges regisztráció"; 
		} else if($cat_alap == "elfelejtett-jelszo"){
			$mod = "frg";
			$all_site_title = "Fantastic store - Elfelejtett jelszó"; 
		} else if($cat_alap == "logout"){
			$mod = "out";
			$all_site_title = "Fantastic store - Kilépés"; 
		} else if($cat_alap == "megrendeles"){
			$mod = "ks_e";
			$all_site_title = "Fantastic store - megrendelés véglegesítése"; 
		} else if($cat_alap == "megrendeles-megerosit"){
			$mod = "ks_er";
			$all_site_title = "Fantastic store - megrendelés megerősítése"; 
		} else if($cat_alap == "megtekintett-termekek"){
			$mod = "mgtk";
			$all_site_title = "Fantastic store - megtekintett termékek"; 
		} else if($cat_alap == "megrendeles-torol"){
			$mod = "rnd_del";
			$all_site_title = "Fantastic store - Rendelés készlettörlés"; 
		} else if($cat_alap == "profil"){
			$mod = "prf";
			$all_site_title = "Fantastic store - Profil"; 
		} else if($cat_alap == "checkout"){
			$mod = "ks_next";
			$all_site_title = "Fantastic store - fizetés"; 
		} else if($cat_alap == "checkoutend"){
			$mod = "ks_end";
			$all_site_title = "Fantastic store - Köszönjük a vásárlást"; 
		} else if($cat_alap == "kapcsolat"){
			$mod = "tr";
			$t_id = 8;
			$all_site_title = "Fantastic store - Kapcsolat"; 
		} else if($cat_alap == "utmutato"){
			$mod = "tr";
			$t_id = 16;
			$all_site_title = "Fantastic store - Vásárlási útmutató"; 
		} else if($cat_alap == "adatvedelem"){
			$mod = "tr";
			$t_id = 17;
			$all_site_title = "Fantastic store - Vásárlási útmutató"; 
		} else if($cat_alap == "vasarlasi-feltetelek"){
			$mod = "tr";
			$t_id = 18;
			$all_site_title = "Fantastic store - Vásárlási feltételek"; 
		} else if($cat_alap == "gyik"){
			$mod = "tr";
			$t_id = 21;
			$all_site_title = "Fantastic store - Gyakori kérdések"; 
		} else if($cat_alap == "szemelyes-atvetel"){
			$mod = "tr";
			$t_id = 20;
			$all_site_title = "Fantastic store - Személyes átvétel"; 
		} else if($cat_alap == "szallitasi-feltetelek"){
			$mod = "tr";
			$t_id = 19;
			$all_site_title = "Fantastic store - Szállítási feltételek"; 
		} else if($cat_alap == "kereses"){
			$mod = "se";
			$all_site_title = "Fantastic store - Keresés"; 
		} else if($cat_alap == "teszt"){
			$mod = "tszt";
			$all_site_title = "Fantastic store - Teszt"; 
		} else if($cat_alap == "zipcreate"){
			$mod = "zip";
			$all_site_title = "Fantastic store - Zip"; 
		} else if($cat_alap == "ro"){
			$_SESSION['nyelv']="ro";
			$mod = "lgch";
			$all_site_title = "Fantastic store";
		} else if($cat_alap == "hu"){
			$_SESSION['nyelv']="hu";
			$mod = "lgch";
			$all_site_title = "Fantastic store";
		} else if($cat_alap == "de"){
			$_SESSION['nyelv']="de";
			$mod = "lgch";
			$all_site_title = "Fantastic store";
		} else {
			
			//kategoria nézet 1 paraméterrel
			include("optika_for_search.php");
			//$tdebug.="(search1)";
		}

	} else if($array_count == 2){
		//$tdebug.="(2)";
		if($cat_alap == "shop"){
			$termeklink = explode('-', $cat_alap_2);
			//echo $termeklink[0];
			$result = mysql_query("SELECT Id,Termek_nev FROM termekek WHERE Id = $termeklink[0]");
			$row = mysql_fetch_assoc($result);
			$t_id = $row['Id'];	
			$termek_nev = $row['Termek_nev'];	

			if(isset($t_id)){
				$mod = "terd";
				$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - Shop - ".$termek_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz1";				
			}
		} else if($cat_alap == "szallitasi-informaciok"){
			//$termeklink = explode('-', $cat_alap_2);
			$t_id = 23;
			$result = mysql_query("SELECT Id,Nev FROM kats_others_tartalom_ebrand WHERE nev_opt = '$cat_alap_2' AND Site_id = 1 AND Fokat = $t_id");
			$row = mysql_fetch_assoc($result);
			$z_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	
			//echo $z_id;
			if(isset($z_id)){
				$mod = "vsoth";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - SZÁLLÍTÁSI információk - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz2";				
			}
		} else if($cat_alap == "termekcsere-garancia"){
			//$termeklink = explode('-', $cat_alap_2);
			$t_id = 26;
			$result = mysql_query("SELECT Id,Nev FROM kats_others_tartalom_ebrand WHERE nev_opt = '$cat_alap_2' AND Site_id = 1 AND Fokat = $t_id");
			$row = mysql_fetch_assoc($result);
			$z_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	
			//echo $z_id;
			if(isset($z_id)){
				$mod = "vsoth";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - SZÁLLÍTÁSI információk - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz3";				
			}
		} else if($cat_alap == "biztonsagos-fizetes"){
			//$termeklink = explode('-', $cat_alap_2);
			$t_id = 29;
			$result = mysql_query("SELECT Id,Nev FROM kats_others_tartalom_ebrand WHERE nev_opt = '$cat_alap_2' AND Site_id = 1 AND Fokat = $t_id");
			$row = mysql_fetch_assoc($result);
			$z_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	
			//echo $z_id;
			if(isset($z_id)){
				$mod = "vsoth";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - Biztonságos-fizetés - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz4";				
			}
		} else if($cat_alap == "ugyfelszolgalat"){
			//$termeklink = explode('-', $cat_alap_2);
			$t_id = 35;
			$result = mysql_query("SELECT Id,Nev FROM kats_others_tartalom_ebrand WHERE nev_opt = '$cat_alap_2' AND Site_id = 1 AND Fokat = $t_id");
			$row = mysql_fetch_assoc($result);
			$z_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	
			//echo $z_id;
			if(isset($z_id)){
				$mod = "vsoth";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Alfatop - SZÁLLÍTÁSI információk - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz5";				
			}
		} else if($cat_alap == "vasarlas-feltetelei"){
			//$termeklink = explode('-', $cat_alap_2);
			$t_id = 20;
			$result = mysql_query("SELECT Id,Nev FROM kats_others_tartalom_ebrand WHERE nev_opt = '$cat_alap_2' AND Site_id = 1 AND Fokat = $t_id");
			$row = mysql_fetch_assoc($result);
			$z_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	
			//echo $z_id;
			if(isset($z_id)){
				$mod = "vsoth";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - SZÁLLÍTÁSI információk - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz6";				
			}
		} else if($cat_alap == "vasarlasi-feltetelek"){
			//$termeklink = explode('-', $cat_alap_2);
			//echo $termeklink[0];
			$result = mysql_query("SELECT Id,Nev FROM static_menu_vasarlas WHERE nev_opt = '$cat_alap_2' AND Fokat = 1");
			$row = mysql_fetch_assoc($result);
			$t_id = $row['Id'];	
			$vasra_nev = $row['Nev'];	

			if(isset($t_id)){
				$mod = "vas";
				//$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - Vásárlási feltételek - ".$vasra_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz7";				
			}
		} else if($cat_alap == "profil"){
			if($cat_alap_2 == "user"){
				$mod = "prf";
				$all_site_title = "Fantastic store - Profil";				
			} else if($cat_alap_2 == "ponts"){
				$mod = "prfpo";
				$all_site_title = "Fantastic store - Profil - Törzsvásárlói pontjaim";				
			} else if($cat_alap_2 == "buys"){
				$mod = "prfbys";
				$all_site_title = "Fantastic store - Profil - Vásárlásaim";				
			} else {
				$mod = "404";
				$all_site_title = "upsz8";				
			}
		} else if($cat_alap == "kampany"){
			$result = mysql_query("SELECT Id,Nev FROM kampany WHERE nev_opt = '$cat_alap_2'");
			$row = mysql_fetch_assoc($result);
			$kmp_id = $row['Id'];
			if(isset($kmp_id)){
				$mod = "kmp";
				$kamp_nev = $row['Nev'];
				$all_site_title = "Fantastic store - ".$kamp_nev."";
				for ($i=0;$i<$array_count;$i++){
					$cat_alap = $path_info['call_parts'][$i];	
					if(!isset($k_id)){
						$result = mysql_query("SELECT Id,Nev FROM kategoria WHERE nev_opt = '$cat_alap'");
						$row = mysql_fetch_assoc($result);
						$k_id = $row['Id'];	
						$kateg_nev = $row['Nev'];
						if($k_id > 0){
							$title_pulsz .= " ".$kateg_nev."";
						}
					}
					if(!isset($gy_id)){
						$result = mysql_query("SELECT Id,Nev FROM gyarto WHERE nev_opt = '$cat_alap'");
						$row = mysql_fetch_assoc($result);
						$gy_id = $row['Id'];	
						$gyarto_nev = $row['Nev'];
						$title_pulsz .= " ".$gyarto_nev."";
					}
					if(!isset($n_id)){
						$result = mysql_query("SELECT Id,Nev FROM nemek WHERE nev_opt = '$cat_alap'");
						$row = mysql_fetch_assoc($result);
						$n_id = $row['Id'];	
						$neme_nev = $row['Nev'];
						if($n_id > 0){
							$title_pulsz .= " ".$neme_nev."";
						}
					}
					if(!isset($me_id)){
						$result = mysql_query("SELECT Id,Nev FROM meret WHERE nev_opt = '$cat_alap'");
						$row = mysql_fetch_assoc($result);
						$me_id = $row['Id'];	
						/*$gyarto_nev = $row['Nev'];
						$title_pulsz .= " ".$gyarto_nev."";*/
					}	
				}		
			} else {
				$mod = "404";
				$all_site_title = "upsz10";				
			}	

		} else {
			
			//kategoria nézet 2 paraméterrel
			include("optika_for_search.php");
			//$tdebug.="(search2)";
		}
	} else if($array_count == 3){
		//$tdebug.="(3)";
		if($cat_alap == "shop"){
			$termeklink = explode('-', $cat_alap_2);
			//echo $termeklink[0];
			$result = mysql_query("SELECT Id,Termek_nev FROM termekek WHERE Id = $termeklink[0]");
			$row = mysql_fetch_assoc($result);
			$t_id = $row['Id'];	
			$termek_nev = $row['Termek_nev'];	

			if(isset($t_id)){
				$mod = "terd";
				$title_nev = $row['Termek_nev'];	
				$all_site_title = "Fantastic store - Shop - ".$termek_nev."";				
			} else {
				$mod = "404";
				$all_site_title = "upsz9";				
			}
		}
		
	}



	if($cat_alap == "cardreturn"){
		$mod = "bkrt";
		$all_site_title = "Fantastic store - Bankkártya"; 
	}

	if($cat_alap == "ipn"){
		$mod = "ipn";
		$all_site_title = "Fantastic store - ipn"; 
	}
	if(!isset($mod)){

		//kategoria nézet ha más paraméter nincs beállítva (ez a kategória nézet  3 paraméterrel)
		//$tdebug.="(nomod_beforelast)";
		include("optika_for_search.php");
	}

}

if(!isset($mod)){
	//$tdebug.="(nomod_last)";	
	$all_site_title = "Fantastic store - Óra és ékszer webáruház";
	$all_site_descr = "Fantastic store - Minőségi, elegáns, sportos órák, karórák és ékszerek webáruháza. Orient, Wenger, Kronaby és sok más világmárka kedvező áron.";
	$all_site_keywords = "karóra, férfi karóra, női karóra, falióra, okosóra, akciós óra, analog óra, automata karóra, ékszer";
}

$myfile = fopen("url_title.log", "a") or die("Unable to open file!");
$actual_link = curPageURL();

if (!isset($all_site_title)) {
	//$tdebug.="(notitle)";
	$txt = "WARN!";
	$uagent=$_SERVER['HTTP_USER_AGENT'];
	$referer=$_SERVER['HTTP_REFERER'];
	$clentip = $_SERVER['HTTP_CLIENT_IP'] ? $_SERVER['HTTP_CLIENT_IP'] : ($_SERVER['HTTP_X_FORWARDED_FOR'] ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);
	$txt .= " ip:($clentip) referer:($referer) agent:($uagent) ";
	
	$uagent=$_SERVER['HTTP_USER_AGENT'];
	$referer=$_SERVER['HTTP_REFERER'];
	$txt .= "link request: ($actual_link) page title: ($all_site_title)";
	fwrite($myfile, "\n". $txt);
	fclose($myfile);
}


$all_site_descr = "Elegáns minőségi vízálló karórák okosórák faliórák ékszerek karkötők divatos ajándékok";
$all_site_descr .= " Orient Wenger Kronaby movado eterna rotary atlantic boccia kappa matheytissot victoria-hyde lee-cooper colori folli-follie pierre-cardin breil muhle daniel-klein freelook kidult ellegirl sergio-tacchini Longines";
$all_site_keywords = $all_site_descr;

$all_site_title .= $tdebug;

?>
