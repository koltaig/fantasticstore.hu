<?
$filenev=pathinfo($_SERVER['PHP_SELF']);
if($filenev['basename']!="index.php")
   die("<font face=\"arial\"><center><br><br>Érvénytelen hívás!</center></font>");
?>
<style>

@media only screen and (max-width: 3000px) {
.menu_cont{width:100%; text-align:center; position: relative;top: 50%;-webkit-transform: translateY(-50%);-ms-transform: translateY(-50%);transform: translateY(-50%);border:0px; z-index:100;}
.menu_subcont{width:100%; max-width:1400px; text-align:center; margin:0px auto;}
.menu_menu{position: relative; margin:0px auto;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  
  text-align:center;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Poppins', sans-serif;
  line-height: 1;
  background: none;
  
  text-align:center;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 0px;
  background: #212121;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
  display:none;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: center;

}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
}
#cssmenu.align-center ul ul {
  text-align: center;
  margin:0px auto;
}
#cssmenu.align-right > ul > li {
  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu ul li{
  font-size: 12px;
  text-decoration: none;
  text-transform: none;
  color: #558b2f;
  font-weight:400;
  text-align:center;
}
#cssmenu > ul > li > a {
  padding: 6px 8px 6px 8px;
  font-size: 21px;
  letter-spacing:0.5px;
  text-decoration: none;
  text-transform: uppercase;
  color: #212121;
  font-weight:normal;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
  margin-right:14px;
}
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
	border-top-left-radius:0px;
	border-top-right-radius:0px;
  background-color:#5393d0;
  color: #FFF;
}
#cssmenu > ul > li.arrow {
  background-color:#FFFFFF;
  color: #212121;
  font-size:23px;
  padding: 12px 0px 12px 0px;
  line-height:1;
  font-weight:300;
  display:block;
}

#cssmenu > ul > li.nowactive a{
  color: #fde835; 
}
#cssmenu > ul > li.nowactive a:hover{
  color: #558b2f; 
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 12px;
  right: 8px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #212121;
  border-right: 1px solid #212121;
  content: "";
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #FFF;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
  top:32px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 34px;
}
#cssmenu ul ul li a {
  padding: 10px 10px 10px 8px;
	border-bottom-left-radius:3px;
	border-bottom-right-radius:3px;
  width: 230px;
  font-family: 'Poppins', sans-serif;
  font-size: 16px;
  text-align:left;
  background: #FFFFFF;
  text-decoration: none;
  color: #000;
  font-weight:400;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #FFF;
  background-color:#5393d0;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 3px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
}

@media only screen and (max-width: 1024px) { 
#cssmenu > ul > li > a {
  padding: 6px 4px 6px 4px;
  font-size: 16px;
  letter-spacing:0.5px;
  text-decoration: none;
  text-transform: uppercase;
  color: #212121;
  font-weight:normal;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
  margin-right:14px;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
  top:28px;
}
}


@media only screen and (max-width: 768px){
.menu_cont{width:100%; text-align:center; background-color:#406050; border-top:3px solid #FFFFFF; border-bottom:3px solid #FFFFFF; height: auto;clear:both;top: 0%;-webkit-transform: translateY(-0%);-ms-transform: translateY(-0%);transform: translateY(-0%);border:0px; z-index:0;}
.menu_subcont{width:100%; max-width:100%; margin:0px auto; height:auto;}
.menu_logo{position: relative ;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_logo img{width:30%; max-width:30%; height:auto; margin:0px auto; border:0px;padding:4px 0px 4px 0px;}
.menu_menu{position: relative;top: 0%;transform: none; float:none; width:100%; margin-left:0%;}
.menu_se_box{position: relative;top: 0%;transform: none; float:none; width:100%; text-align:center;}
.menu_aktion{color:#fffc00;}
#cssmenu,
#cssmenu ul,
#cssmenu ul li,
#cssmenu ul li a,
#cssmenu #menu-button {
  margin: 0;
  padding: 0;
  border: 0;
  list-style: none;
  line-height: 1;
  display: block;
  position: relative;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
#cssmenu:after,
#cssmenu > ul:after {
  content: ".";
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
#cssmenu #menu-button {
  display: none;
}
#cssmenu {
  width: auto;
  font-family: 'Playfair Display';
  line-height: 1;
  background: none;
}
#menu-line {
  position: absolute;
  top: 0;
  left: 0;
  height: 3px;
  background: #9c0000;
  -webkit-transition: all 0.25s ease-out;
  -moz-transition: all 0.25s ease-out;
  -ms-transition: all 0.25s ease-out;
  -o-transition: all 0.25s ease-out;
  transition: all 0.25s ease-out;
}
#cssmenu > ul > li {
  float: left;
}
#cssmenu.align-center > ul {
  font-size: 0;
  text-align: left;
}
#cssmenu.align-center > ul > li {
  display: inline-block;
  float: none;
  text-align: left;
}
#cssmenu.align-center ul ul {
  text-align: left;
}
#cssmenu.align-right > ul > li {

  float: right;
}
#cssmenu.align-right ul ul {
  text-align: right;
}
#cssmenu ul li{
  font-size: 27px;
  text-decoration: none;
  text-transform: uppercase;
  color: #007f3d;
  font-weight:400;
  text-align:center;
}
#cssmenu > ul > li > a {
  padding: 8px;
  font-size: 14px;
  text-decoration: none;
  text-transform: uppercase;
  color: #7b7b7b;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
  text-align: left;
}

#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
	border-top-left-radius:0px;
	border-top-right-radius:0px;
  background-color:#9f9f9f;
  color: #7b7b7b;
}
#cssmenu > ul > li.nowactive a{
  color: #2aa666; 
}
#cssmenu > ul > li.has-sub > a {
  padding-right: 25px;
}
#cssmenu > ul > li.has-sub > a::after {
  position: absolute;
  top: 21px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  content: "";
  -webkit-transform: rotate(45deg);
  -moz-transform: rotate(45deg);
  -ms-transform: rotate(45deg);
  -o-transform: rotate(45deg);
  transform: rotate(45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu > ul > li.has-sub:hover > a::after {
  border-color: #fffc00;
}
#cssmenu ul ul {
  position: absolute;
  left: -9999px;
  top:0px;
}
#cssmenu li:hover > ul {
  left: auto;
}
#cssmenu.align-right li:hover > ul {
  right: 0;
}
#cssmenu ul ul ul {
  margin-left: 100%;
  top: 0;
}
#cssmenu.align-right ul ul ul {
  margin-left: 0;
  margin-right: 100%;
}
#cssmenu ul ul li {
  height: 0;
  -webkit-transition: height .2s ease;
  -moz-transition: height .2s ease;
  -ms-transition: height .2s ease;
  -o-transition: height .2s ease;
  transition: height .2s ease;
}
#cssmenu ul li:hover > ul > li {
  height: 32px;
}
#cssmenu ul ul li a {
  padding: 10px 20px;
  width: 160px;
  font-size: 14px;
  background: #333333;
  text-decoration: none;
  color: #dddddd;
  -webkit-transition: color .2s ease;
  -moz-transition: color .2s ease;
  -ms-transition: color .2s ease;
  -o-transition: color .2s ease;
  transition: color .2s ease;
}
#cssmenu ul ul li:hover > a,
#cssmenu ul ul li a:hover {
  color: #7b7b7b;
}
#cssmenu ul ul li.has-sub > a::after {
  position: absolute;
  top: 13px;
  right: 10px;
  width: 4px;
  height: 4px;
  border-bottom: 1px solid #dddddd;
  border-right: 1px solid #dddddd;
  content: "";
  -webkit-transform: rotate(-45deg);
  -moz-transform: rotate(-45deg);
  -ms-transform: rotate(-45deg);
  -o-transform: rotate(-45deg);
  transform: rotate(-45deg);
  -webkit-transition: border-color 0.2s ease;
  -moz-transition: border-color 0.2s ease;
  -ms-transition: border-color 0.2s ease;
  -o-transition: border-color 0.2s ease;
  transition: border-color 0.2s ease;
}
#cssmenu.align-right ul ul li.has-sub > a::after {
  right: auto;
  left: 10px;
  border-bottom: 0;
  border-right: 0;
  border-top: 1px solid #dddddd;
  border-left: 1px solid #dddddd;
}
#cssmenu ul ul li.has-sub:hover > a::after {
  border-color: #ffffff;
}
  #cssmenu {
    width: 100%;
	
  }
  #cssmenu ul {
    width: 100%;
    display: none;
  }
  #cssmenu.align-center > ul,
  #cssmenu.align-right ul ul {
    text-align: center;
  }
  #cssmenu ul li,
  #cssmenu ul ul li,
  #cssmenu ul li:hover > ul > li {
  	color:#000000;
	text-align:left;
	font-size:10px;
    width: 100%;
    height: auto;
    border-top: 1px dashed rgba(120, 120, 120, 0.45);
  }
  #cssmenu ul li a,
  #cssmenu ul ul li a {
  	color:#000000;
	font-size:16px;
	padding:15px 0px 15px 5px;
    width: 100%;
	background-color:#FFFFFF;
  }
  #cssmenu ul li a:hover {
  	color:#7b7b7b;
  }
  #cssmenu > ul > li,
  #cssmenu.align-center > ul > li,
  #cssmenu.align-right > ul > li {
    float: none;
    display: block;
  }

  #cssmenu ul ul li:hover > a,
  #cssmenu ul ul li a:hover {
    color: #7b7b7b;
  }
  #cssmenu ul ul ul li a {
    padding-left: 40px;
  }
  #cssmenu ul ul,
  #cssmenu ul ul ul {
    position: relative;
    left: 0;
    right: auto;
    width: 100%;
    margin: 0;
  }
  #cssmenu > ul > li.has-sub > a::after,
  #cssmenu ul ul li.has-sub > a::after {
    display: none;
  }
  #menu-line {
    display: none;
  }
  #cssmenu #menu-button {
    display: block;
    padding: 15px 0px 15px 3px;
    color: #FFFFFF;
    cursor: pointer;
    font-size: 18px;
    text-transform: uppercase;
	background-color:#406050;
	text-align:center;
  }
  #cssmenu #menu-button::after {
    content: '';
    position: absolute;
    top: 16px;
    right: 20px;
    display: block;
    width: 15px;
    height: 2px;
    background: #FFFFFF;
  }
  #cssmenu #menu-button::before {
    content: '';
    position: absolute;
    top: 21px;
    right: 20px;
    display: block;
    width: 15px;
    height: 3px;
    border-top: 2px solid #FFFFFF;
    border-bottom: 2px solid #FFFFFF;
  }
  #cssmenu .submenu-button {
    position: absolute;
    z-index: 10;
    right: 0;
    top: 0;
    display: block;
    border-left: 1px solid rgba(120, 120, 120, 0.15);
    height: 44px;
    width: 52px;
    cursor: pointer;
  }
  #cssmenu .submenu-button::after {
    content: '';
    position: absolute;
    top: 17px;
    left: 23px;
    display: block;
    width: 1px;
    height: 11px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button::before {
    content: '';
    position: absolute;
    left: 18px;
    top: 22px;
    display: block;
    width: 11px;
    height: 1px;
    background: #000000;
    z-index: 99;
  }
  #cssmenu .submenu-button.submenu-opened:after {
    display: none;
  }
#cssmenu > ul > li:hover > a,
#cssmenu > ul > li.active > a {
  color: #FFFFFF;
}
}

</style>

<div class="menu_cont">
	<div class="menu_subcont">
        <div class="menu_menu" style="z-index:100;">
            <div id='cssmenu'>

                <ul>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ferfi/ora/">Férfi óra</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/om/ceas/">Om Ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/herren/uhr/">Herren Uhr</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/noi/ora/">Női óra</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/femeie/ceas/">Femeie Ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/damen/uhr/">Damen Uhr</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/tovabbi-orak">Egyéb órák</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/altii-ceas">Alții ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/andere-uhren">Andere Uhren</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/oraszij">Óraszíj</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/curea-de-ceas">Curea de ceas</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/uhrenarmband">Uhrenarmband</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ekszer">Ékszer</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/bijuterie">Bijuterie</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/juwel">Juwel</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/dragakoves-ekszer">Drágaköves ékszer</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/dragakoves-ekszer">Drágaköves ékszer</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/dragakoves-ekszer">Drágaköves ékszer</a><? } ?></li>
    		  <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/karikagyuru">Karikagyűrű</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/kgy">KGY</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/kgy">KGY</a><? } ?></li>
		      <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/eljegyzesigyuru">Eljegyzesi gyűrű</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/egy">EGY</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/egy">EGY</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/ongyujto">Öngyújtó</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/bricheta">Brichetă</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/Feuerzeug">Feuerzeug</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/szemuveg">Szemüveg</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/ochelari">Ochelari</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/brille">Brille</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/penztarca">Pénztárca</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/portofel">Portofel</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/brieftasche">Brieftasche</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/taskak">Táska</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/taskak">Táskák</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/taskak">Táskák</a><? } ?></li>
          <li><? if($_SESSION['nyelv'] == "hu"){ ?><a href="/parfum">Parfüm</a><? } else if($_SESSION['nyelv'] == "ro"){ ?><a href="/parfum">parfum</a><? } if($_SESSION['nyelv'] == "de"){ ?><a href="/parfum">Parfum</a><? } ?></li>
                </ul>
            </div>        
        </div>

    </div>
</div>

